<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table){
            $table->increments('id');
            $table->string('slug')->unique();
            $table->boolean('status')->nullable();
            $table->string('meta_description')->nullable();
            $table->integer('user_id'); //->unsigned();
            $table->integer('category_id')->nullable();
            $table->string('title');
            $table->string('post_cover');
            $table->string('step_count'); //->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
