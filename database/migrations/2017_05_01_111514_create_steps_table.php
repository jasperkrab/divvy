<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id'); //->unsigned();
            $table->integer('step_nr'); //->unsigned();
            $table->string('title')->nullable();
            $table->text('body')->nullable();
            $table->timestamps();
        });

        // Schema::table('steps', function ($table){
        //     $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropForeign(['post_id']);
        Schema::dropIfExists('steps');
    }
}
