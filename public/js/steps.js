$(document).ready(function(){

	var counter = 4;

	$("#TextBoxesGroup").sortable({
		stop: function (event, ui) {
			var children = $(this).children("div.stepbox");
			children.each(function (index) {
				var l = $(this).find("label");
				var text = "Step " + (index + 1);
				l.text(text);
			});
		}
	});

	//add function
    $("#addButton").click(function () {

		if(counter > 25) {
			alert("Only 25 textboxes allowed!");
			return false;
		}

		var newTextBoxDiv = $(document.createElement('div'))
			.attr("id", 'TextBoxDiv' + counter)
			.attr("class", 'stepbox form-group'); //class toevoeging

		newTextBoxDiv.after().html('<label>Step&nbsp;'+ counter + '&nbsp;</label>' +
			  '<textarea class="form-control" rows="4" placeholder="Omschrijving" name="textbox' + counter +
			  '" id="textbox' + counter + '" value="" >');

		newTextBoxDiv.appendTo("#TextBoxesGroup"); //voegt de nieuwe div toe aan deze class of id
		
		counter++;
	});

	//remove function
    $("#removeButton").click(function () {
		if(counter==2) {
			alert("No more textbox to remove");
			return false;
		}

		counter--;

		$("#TextBoxDiv" + counter).remove();
	});

	$("#getButtonValue").click(function () {

		var msg = '';
		for(i=1; i<counter; i++){
			msg += "\n Stap " + i + $('#textbox' + i).val();
		}
    	alert(msg);
	});
	
});