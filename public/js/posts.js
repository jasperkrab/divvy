$(document).ready(function(){

	$("#step-box-group").sortable({
		stop: function (event, ui) {
			var children = $(this).children("div.step-box");
			children.each(function (index) {
				var l = $(this).find(".step-box-title");
				var html = "<h4>Stap " + (index + 1) + "</h4>";
				l.html(html);
			});
		}
	});

	//add function
    $("#add-step").click(function () {

		if(counter > 30) {
			alert("Helaas is het niet toegestaan meer dan 30 stappen te gebruiken.");
			return false;
		}

		var newStep = $(document.createElement('div'))
			.attr("id", 'step-box-' + counter)
			.attr("class", 'card step-box'); //class toevoeging

		newStep.after().html('<label>Step&nbsp;'+ counter + '&nbsp;</label>' +
			  '<textarea class="form-control" rows="4" placeholder="Omschrijving" name="textbox' + counter +
			  '" id="textbox' + counter + '" value="" >');

		newStep.appendTo("#step-box-group"); //voegt de nieuwe div toe aan deze class of id
		
		counter++;
	});

	//remove function
    $("#removeButton").click(function () {
		if(counter==1) {
			alert("Je moet minimaal één stap omschrijven.");
			return false;
		}

		counter--;

		$("#step-box" + counter).remove();
	});


	// $("#getValueButton").click(function () {

	// 	var msg = '';
	// 	for(i=1; i<counter; i++){
	// 		msg += "\n Stap " + i + $('#step-title' + i).val();
	// 	}
 	//    	alert(msg);
	// });

});