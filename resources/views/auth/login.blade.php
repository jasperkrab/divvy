@extends('layouts.main')

@section('title', '| Login')

@section('stylesheets')
    <!-- page exclusive styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-social.css') }}">
@endsection

@section('content')

    <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mailadres</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                    @include('errors.partials._formerrors', ['errorName' => 'email'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Wachtwoord</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>
                                    @include('errors.partials._formerrors', ['errorName' => 'password'])
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Onthoud mij
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Wachtwoord vergeten?
                                    </a>
                                </div>
                            </div>
                        </form>
                       {{--  <hr>
                        <form>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4 social-col">
                                <a class="btn btn-block btn-social btn-facebook">
                                        <span class="fa fa-facebook"></span> Log in met Facebook
                                    </a>
                                    <a class="btn btn-block btn-social btn-google">
                                        <span class="fa fa-google"></span> Log in met Google
                                    </a>
                                </div>
                            </div>
                        </form> --}}
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    <!-- page exclusive scripts -->
@stop
