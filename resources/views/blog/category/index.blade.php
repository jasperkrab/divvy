@extends('layouts.main')

@section('title', "| $category")

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')
	<!-- content -->
    <div class="row">
        <div class="col-md-12">
            <h3>{{ $category }} posts</h3>
            <hr class="home-hr">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        	@foreach ($posts as $post)
                @include('posts.partials._post_tumb')
        	@endforeach
        </div>
    </div>
   {{--  <div class="row">
        <div class="col-md-12">   
            <hr class="home-hr">
            <div><h5 class="pull-right">Pagina {{ $posts->currentPage() }} van {{ $posts->lastPage() }}</h5></div>
            <div class="col-md-12">
                <div class="text-center">
                    {!! $posts->links() !!}
                </div>
            </div>           
        </div>
    </div> --}}

@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop