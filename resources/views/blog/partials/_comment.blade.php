<div class="comment">
    <div class="comment-tumb">
        <a href="#"><img src="{{ asset('storage/img/avatars/' . $commentType->user->avatar) }}"></a>
    </div>
    <div class="comment-body">
        <h4>{{ ucfirst($commentType->user->username) }} <small>{{ $commentType->created_at->diffForHumans() }}</small>
        @if (auth()->user() == $commentType->user)
            <div class="pull-right comment-menu hidden" data-com_active="0">
                <form method="POST" role="form" action="{{ route('comments.destroy', $commentType->id) }}" class="d-inline">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button class="btn btn-danger btn-xs delete-comment" type="submit" title="Reactie verwijderen"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    {{-- edit comment button --}}
                    <a href="#_" class="btn btn-warning btn-xs d-inline edit-comment" title="Reactie bewerken"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                </form>
            </div>
        @else
            <div class="pull-right comment-menu hidden">
            <a href="#_" class="btn btn-danger btn-xs d-inline" title="Spam?"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></a>
            </div>
        @endif
        </h4>
        <p class="com-text">{{ $commentType->body }}</p>

        {{-- Edit form --}}
        <div class="com-edit-form hidden">
            <form method="POST" role="form" action="{{ route('comments.update', $commentType->id) }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group{{ $errors->has('edit_body') ? ' has-error' : '' }}">
                    <textarea name="edit_body" id="edit_body" class="form-control" class="comment-box">{{ $commentType->body }}</textarea>
                </div>
                <div class="form-group pull-right">
                    <button type="button" class="btn btn-sm btn-warning cancel-edit">Annuleren</button>
                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i> Bewerken</button>
                </div>
            </form>
            @include('errors.partials._formerrors', ['errorName' => 'edit_body'])
        </div>

        <a href="#_" class="leave-comment"><small>Beantwoorden {{-- &nbsp; &bull; --}}</small></a>
        {{-- <a href=""><span class="glyphicon glyphicon-thumbs-up"></span></a>
        <a href=""><span class="glyphicon glyphicon-thumbs-down"></span></a> --}}
        <div class="reply-form hidden">
            {{-- Reply form --}}
            <hr>
            <form method="POST" role="form" action="{{ route('comments.reply', [$comment->post->slug, $comment->id]) }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('reply_body') ? ' has-error' : '' }}">
                    <textarea name="reply_body" class="form-control" class="comment-box" placeholder="Schrijf hier een reactie..." required></textarea>
                </div>
                <div class="form-group pull-right">
                    <button type="button" class="btn btn-sm btn-warning cancel-reply">Annuleren</button>
                    <button type="submit" class="btn btn-sm btn-success">Reageren</button>
                </div>
            </form>
            @include('errors.partials._formerrors', ['errorName' => 'reply_body'])
        </div>
    </div>
</div>