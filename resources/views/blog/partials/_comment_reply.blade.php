<div class="comment">
    <div class="comment-tumb">
        <a href="#"><img src="{{ asset('storage/img/avatars/' . $reply->user->avatar) }}"></a>
    </div>
    <div class="comment-body">
        @if (auth()->user() == $reply->user)
            <div class="pull-right">
                <form method="POST" role="form" action="{{ route('comments.destroy', $reply->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button class="btn btn-danger btn-xs hidden delete-comment" type="submit">Verwijderen</button>
                </form>
            </div>
        @endif
        <h4>{{ $reply->user->username }} <small>{{ $reply->created_at->diffForHumans() }}</small></h4>
        <p>
            {{ $reply->body }}
        </p>
        <a href="#_" class="leavecomment"><small>Beantwoorden {{-- &nbsp; &bull; --}}</small></a>
        {{-- <a href=""><span class="glyphicon glyphicon-thumbs-up"></span></a>
        <a href=""><span class="glyphicon glyphicon-thumbs-down"></span></a> --}}
        <div class="reply-form">
            <div class="comment-reply">
                <form method="POST" role="form" action="{{ route('comments.reply', [$comment->post->slug, $comment->id]) }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('reply_body') ? ' has-error' : '' }}">
                        <textarea name="reply_body" class="form-control" class="comment-box" placeholder="Schrijf hier een reactie..." required></textarea>
                    </div>
                    <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-warning pull-right">Reageren</button>
                    </div>
                </form>
                @include('errors.partials._formerrors', ['errorName' => 'reply_body'])
            </div>
        </div>
    </div>
    <hr>
</div>