@extends('layouts.main')

@section('title', "| $tag")

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')
	<!-- content -->
    <div class="row">
        <div class="col-md-12">
            <h3>{{ $tag }} posts</h3>
            <hr class="home-hr">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        	@foreach ($posts as $post)
                @include('posts.partials._post_tumb')
        	@endforeach
        </div>
    </div>

@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop