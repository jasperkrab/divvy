@extends('layouts.main')

@section('title', '| All divvy\'s')

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')
	<!-- content -->
    <div class="row">
        <div class="col-md-12">
            <div><h5 class="pull-right">Pagina {{ $posts->currentPage() }} van {{ $posts->lastPage() }}</h5></div>
            <hr>
        	@foreach ($posts as $post)
                @include('posts.partials._post_tumb')
        	@endforeach
            <hr>
            <div class="col-md-12">
                <div class="text-center">
                    {!! $posts->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop