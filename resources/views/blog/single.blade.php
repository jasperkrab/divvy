@extends('layouts.main')

@section('title', "| $post->title")

@section('stylesheets')
	<!-- page exclusive styles -->
    <link rel="stylesheet" href="{{ asset('css/posts.css') }}">

    <!--script voor reply form-->
    <script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <!-- Post header -->
            <div class="card card-success">
                <div class="post-title">
                    <h2>{{ $post->title }}<small> Door <a href="">{{ucfirst($post->user->username)}}</a> in <a href="">Houtbewerking</a></small></h2>
                </div>
            </div>

            <div class="card">
                <div class="post-cover">
                    <div class="image">
                        <img src="{{ asset('img/post/cover.jpg') }}">
                    </div>
                    <div class="container">
                        <a class="btn cover-btn img-circle pull-right" title="Report this post"><i class="fa fa-flag" aria-hidden="true"></i></a>
                        <a class="btn cover-btn circle pull-right" title="Favourite"><i class="fa fa-heart" aria-hidden="true"></i></a>
                        <a class="btn cover-btn circle pull-right" title="Add to your collection"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <a class="btn cover-btn circle pull-right" title="Share"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                        
                    </div>
                </div>

                <div class="post-navbar">
                    <nav class="navbar nav" id="nav">
                        <form class="form-inline center">
                            <button class="btn btn-primary" type="button">Download</button>
                            <button type="button" class="btn btn-default">Introduction</button>
                            <a class="btn btn-default" href="#step-1">1</a>
                            <button class="btn btn-default" type="button">2</button>
                            <button class="btn btn-default" type="button">3</button>
                            <button class="btn btn-default" type="button">...</button>
                        </form>
                    </nav>
                </div>
                    
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <!-- Post Body -->
            <div class="post-body">

                <!-- Post step (preview)  -->
                @foreach($post->steps as $step)
                    <div class="card">
                        <div class="step-header" id="step-1">
                            <div class="">
                                <img src="{{ asset('img/post/step1.jpg') }}">
                            </div>
                        </div>

                        <div class="step-content">

                            <h4>{{ $step->title }}</h4>
                            <hr>
                            <p>{{ $step->body }}</p>
                        </div>
                    </div>
                @endforeach

                <!-- Post footer -->
                <div class="card">
                    <div class="post-footer">
                        <ul class="pager">
                            <li><a href="#">←  Prev</a></li>
                            <li><a href="#">All Steps</a></li>
                            <li><a href="#">Next  →</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <!-- Post info -->
         <div class="col-md-4">
            <div class="card post-well">
                <div class="center">
                    <h3>Over deze tutorial</h3>
                    <hr>
                    <div class="post-meta">
                        <ul class="list-inline">
                            <li alt="Views"><span class="glyphicon glyphicon-eye-open"></span> 114</li>
                            <li alt="Comments"><span class="glyphicon glyphicon-comment"></span> 4</li>
                            <li alt="Downloads"><span class="glyphicon glyphicon-download-alt"></span> 0</li>
                            <li alt="Likes"><span class="glyphicon glyphicon-heart"></span> 22</li>
                        </ul>
                        <hr>
                    </div>
                    <div class="well-info">
                        <div class="well-left">
                            <p>
                                <b>Categorie:</b>
                                <br>
                                <b>Gepubliceerd:</b>
                                <br>
                                <b>Weergaven:</b>
                                <br>
                                <b>Favorieten:&nbsp;</b>
                            </p>
                        </div>
                        <div class="well-right">
                            <p>
                                {{ $post->category->category }}
                                <br>
                                {{ date('t M Y' , strtotime($post->updated_at)) }}
                                <br>
                                8.442
                                <br>
                                50
                            </p>
                        </div>
                    </div>
                     
                    <div class="post-auth">
                        <hr>
                        <h4>Auteur:</h4>
                        <div class="auth-tumb well-left">
                            <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        </div>
                        <div class="auth-body well-right">
                            <h4>{{ ucfirst($post->user->username) }}</h4>
                            <a href="" class="btn btn-warning">Volgen</a>
                        </div>
                    </div>   

                    <div class="">
                        <hr>
                        <h4>Tags:</h4>
                        <h4>
                            @if(count($post->tags))
                                @foreach($post->tags as $tag)
                                    <a href="{{ route('tag.index', $tag->tag) }}"><span class="label label-danger">{{ $tag->tag }}</span></a>
                                @endforeach
                            @endif
                        </h4>
                    </div>

                    <div class="post-social center">
                        <hr>
                        <h4>Deel op:</h4>
                        <a class="btn btn-social btn-facebook"><i class="fa fa-facebook-square"></i></a>
                        <a class="btn btn-social btn-facebook"><i class="fa fa-twitter-square"></i></a>
                        <a class="btn btn-social btn-facebook"><i class="fa fa-google-plus-square"></i></a>
                        <a class="btn btn-social btn-facebook"><i class="fa fa-pinterest-square"></i></a>
                    </div>

                    <div class="post-related">
                        <hr>
                        <h4>Gerelateerd:</h4>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                    </div>

                    <hr>
                    <div>
                        <button class="btn btn-block btn-warning" onclick="history.go(-1)">Terug</button>
                    </div>

                    <hr>
                </div>
            </div>
        </div>
    </div>

    <!-- Post comments -->
    <div class="row">
        <div class="col-md-8">
            
                <div class="card post-comments card-success">
                    <h3>Voeg een reactie toe</h3>
            
                    <form method="POST" role="form" action="{{ route('comments.store', $post->slug) }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                            <textarea name="body" class="form-control" id="comment-box" placeholder="Schrijf hier een reactie..." required></textarea>
                        </div>
                        <div class="form-group">
                        <button type="submit" class="btn btn-warning pull-right hidden" id="btn-comment">
                            Reageren
                        </button>
                        </div>
                    </form>
                    @include('errors.partials._formerrors', ['errorName' => 'body'])
                </div>

            <div class="post post-comments">
                <h3>Reacties</h3>
                <hr>
                @foreach ($comments as $comment)
                    @if(!$comment->parent_comment_id)
                        @include('blog.partials._comment', ['commentType' => $comment])

                        @if ($comment->replies)
                            @foreach($comment->replies as $reply)
                                <div style="margin-left: 30px;">
                                    @include('blog.partials._comment', ['commentType' => $reply])
                                </div>
                            @endforeach
                        @endif
                        <hr>
                    @endif
                @endforeach
                <div class="post-comments-footer">
                    <button class="btn btn-default btn-block">Meer reacties</button>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->

   {{--  <script type="text/javascript">
        //fixing second nav to header
        $(function() {
            $('#nav-wrapper').height($("#nav").height());
            
            $('#nav').affix({
                offset: { top: $('#nav').offset().top }
            });
        });
    </script> --}}

    <script type="text/javascript">
        $(document).ready(function() {
            //reactie add delete button
            $("#comment-box").focus(function() {
                $(this).height('65px');
                $('#btn-comment').fadeIn( 500 ).removeClass("hidden");
            });

            //comment box
            $("#comment-box").focusout(function() {
                setTimeout(function() {
                    $("#comment-box").height('20px');
                    $('#btn-comment').addClass("hidden");
                }, 500);        
            });

            //verbergen overige forms
            function resetComments(){
                $(".com-edit-form").addClass("hidden");
                $('.com-text').removeClass("hidden");
                $(".reply-form").addClass("hidden");

                //reset comment menu
                $('.comment-menu').data('com_active', 0);
            }

            // function showForm(){
            //     $(this).fadeIn(500).addClass("hidden");
            // }

            // function hideForm(){
            //     $(this).fadeIn(500).removeClass("hidden");
            // }

            //reply form
            $(".leave-comment").click(function () {
                resetComments();

                $(this).next(".reply-form").fadeIn(500).removeClass("hidden");
                $(this).closest('.comment-body').children(".com-edit-form").addClass("hidden");
                $(this).closest('.comment-body').children(".com-text").fadeIn(500).removeClass("hidden");
            });

            $(".cancel-reply").click(function () {
                resetComments();
            });

            //edit form
            $(".edit-comment").click(function () {
                resetComments();

                //make menu active
                $(this).closest('.comment-menu').data('com_active', 1);
                
                //make menu active
                $(this).closest('.comment-menu').addClass("hidden");
                $(this).closest('.comment-body').find('.com-text').addClass("hidden");
                $(this).closest('.comment-body').find('.com-edit-form').removeClass("hidden");
            });

            $(".cancel-edit").click(function () {
                resetComments();
            });

            //delete comments
            // $('.comment').hover(function () {
            //     var formActive = $(this).find('.comment-menu').data('com_active');
            //     var commentMenu = $(this).find('.comment-menu');

            //     if( formActive == 0){
            //         console.log('not active');
            //         commentMenu.toggleClass("hidden");
            //     }
            //     function () {
            //         console.log('hover out');
            //     }

            //comment hover function
            $('.comment').hover(function () {
                var formActive = $(this).find('.comment-menu').data('com_active');

                if( formActive == 0){
                    $(this).find('.comment-menu').removeClass("hidden");
                };
            }, function () {
                $(this).find('.comment-menu').addClass("hidden");   
            });

        });
    </script>
@stop