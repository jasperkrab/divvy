<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('layouts.partials._head')
        @yield('stylesheets')
    </head>
    
    <body>
        <div id="divvy">
            @include('layouts.partials._navigation')

            <div id="body">
                @include('flash::message')
                
                @yield('cover')

                <div class="container">
                     @yield('content')
                </div>
              
            </div>

            <div class="footer">
                @include('layouts.partials._footer')
            </div>

            @include('layouts.partials._javascript')

            @yield('scripts')
        </div>
    </body>
</html>