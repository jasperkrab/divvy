<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-1 col-sm-3">
                <div class="info">
                    <h5 class="title">Divvy</h5>
                    <nav>
                        <ul>
                            <li><a href="{{ route('about') }}">Over ons</a></li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                            <li><a href="#">Vacatures</a></li>
                            <li><a href="#">Privacy</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="info">
                    <h5 class="title">Help and Support</h5>
                    <nav>
                        <ul>
                            <li>
                                <a href="{{ route('howto') }}">Hoe het werkt</a>
                            </li>
                            <li>
                                <a href="#">Voorwaarden</a>
                            </li>
                            <li>
                                <a href="#">Disclaimer</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            
            <div class="col-md-2 col-sm-3">
                <div class="info">
                    <h5 class="title">Volg ons op</h5>
                    <nav>
                        <ul>
                            <li>
                                <a href="https://www.facebook.com/Divvy-1868597343399734/" target="_blank" class="btn btn-social btn-facebook">
                                    <i class="fa fa-facebook-square"></i> Facebook
                                </a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-social btn-google">
                                    <i class="fa fa-google-plus-square"></i> Google+
                                </a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-social btn-twitter">
                                    <i class="fa fa-twitter"></i> Twitter
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="info">
                    <h5 class="title">Nieuwsbrief</h5>
                    <div>
                        Ontvang nieuws over Divvy en verschillende doe-het-zelf projecten!
                        <form>
                            <div class="input-group">
                                <input type="email" name="newsletteremail" placeholder="Email" class="form-control">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-info btn-danger">Subscribe!</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="copyright">
           Copyright © 2017 Divvy
        </div>
    </div>
</footer>

<a id="back-to-top" href="#" class="btn btn-danger btn-lg back-to-top" role="button" title="Click to return to the top page" data-toggle="tooltip" data-placement="left"><i class="fa fa-chevron-up" aria-hidden="true"></span></i>
