<nav class="navbar navbar-default fixed-top main-navbar" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">
        <img alt="Divvy" src="{{ asset('img/logo/nav_logo.jpg') }}">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
        <li class="{{ Request::is('/') ? "active" : "" }}"><a href="/">Home</a></li>
    </ul>
    <form class="navbar-form navbar-left">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-danger" id="search-btn">Search <i class="fa fa-spinner faa-spin animated" style="display: none;" aria-hidden="true"></i></button>
    </form>

    <!-- login -->
    <ul class="nav navbar-nav navbar-right">
        @if (Auth::guest())
          <li><a href="{{ route('login') }}">Inloggen</a></li>
          <li><a href="{{ route('register') }}">Aanmelden</a></li>
        @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ ucfirst(Auth::user()->username) }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    @if(Auth::user()->hasRole('admin'))      
                        <li class="{{ Request::is('admin') ? "active" : "" }}"><a href="{{ route('admin') }}">Admin Panel</a></li>
                        <li class="{{ Request::is('admin/categories') ? "active" : "" }}"><a href="{{ route('categories.index') }}">Categorieën</a></li>
                        <li class="{{ Request::is('admin/tags') ? "active" : "" }}"><a href="{{ route('tags.index') }}">Tags</a></li>
                        <li role="separator" class="divider"></li>
                    @endif
                    <li class="{{ Request::is( Auth::user()->username . '/profiel') ? "active" : "" }}"><a href="{{ route('profile', Auth::user()->username) }}">Mijn profiel</a></li>
                    <li class="{{ Request::is( 'account') ? "active" : "" }}"><a href="{{ route('account', Auth::user()->username) }}">Account</a></li>
                    <li class="{{ Request::is( 'berichten') ? "active" : "" }}"><a href="{{ route('messages') }}"><i class="fa fa-envelope" aria-hidden="true"></i> Berichten</a></li>
                    
                    <li role="separator" class="divider"></li>
                    <li class="{{ Request::is( Auth::user()->username . '/profiel/posts') ? "active" : "" }}"><a href="{{ route('user.posts', Auth::user()->username) }}">Posts <span class="badge badge-success pull-right">{{ count(Auth::user()->posts->where('status', 1)) }}</span></a></li>
                    <li class="{{ Request::is( 'drafts') ? "active" : "" }}"><a href="{{ route('user.drafts') }}">Drafts <span class="badge badge-warning pull-right">{{ count(Auth::user()->posts->where('status', 0)) }}</span></a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Help</a></li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a href="{{ route('logout') }}" class="btn btn-danger" 
                            onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            Uitloggen
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
          @endif
    </ul>
    
    <div class="navbar-form navbar-right">
        <button class="btn btn-success" data-toggle="modal" data-target="#create-post-modal">Plaats een post</button>
    </div>


    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!-- new post modal -->
<div class="modal fade" id="create-post-modal" tabindex="-1" role="dialog" aria-labelledby="createPostModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="" id="new-post-form" method="POST" role="form" action="{{ route('posts.store') }}">
                {{ csrf_field() }}

                <div class="modal-header" style="background: #f2f2f2;">
                    <div class="col-sm-8">
                        <h3>Leuk dat je een Divvy gaat maken!</h3>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{ asset('img/divvytalk.jpg') }}" class="img-responsive">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">                
                        <label for="title"><h4>Geef je project een titel</h4></label>
                        <input type="text" class="form-control input-lg" id="title" name="title" placeholder="Geef je project hier een titel">
                        @include('errors.partials._formerrors', ['errorName' => 'title'])
                    </div>
                </div>
                <div class="modal-footer">
                    <!-- footer btn's -->
                    <button class="btn btn-warning">Terug</button>
                    <button class="btn btn-lg btn-success" type="submit" id="store-new-post">Start met schrijven!</button>
                </div>
            </form>
        </div>
    </div>
</div>
