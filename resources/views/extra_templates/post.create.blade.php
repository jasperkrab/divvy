@extends('main')

@section('title', '| Create new post')

@section('stylesheets')
	<!-- page exclusive styles -->
	<link rel="stylesheet" href="{{ asset('css/posts.css') }}">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-tour.css') }}">

	<!--standaard jquery staat in head-->
	
	<!--script voor drag and drop functies-->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<!--script voor de add and remove functie-->
	<script src="{{ asset('js/posts.js') }}"></script>
@endsection

@section('content')
	

	{{-- Modal --}}
	<div class="modal out fade" id="welcome-modal">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">Leuk dat je een Divvy gaat maken!</h4>
				</div>
				<div class="modal-footer center">
					<a href="" class="btn btn-success" data-dismiss="modal" data-tour>Hoe werkt het?</a>
					<a href="" class="btn btn-danger" data-dismiss="modal">Begrepen!</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			{{-- title --}}
			<div class="card card-header card-top card-success">
				<form method="POST" action="/upload">
					<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
						
						<div class="alert show alert-warning alert-dismissable alert-important fade in alert-label">
						    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
							<label for="title">Geef je project hier een titel.</label>
						</div>
						<input type="text" class="form-control input-lg" id="title" name="title" value="{{ old('title') }}" placeholder="Geef je project hier een titel">

						@if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
					</div>
				</form>
			</div>

			{{-- dropzone --}}
			<div id="dropzone">
				<div class="post feat-img-box">
					<form method="POST" class="dropzone needsclick dz-clickable" id="demo-upload">
					{{ csrf_field() }}
						<div class="dz-message needsclick">
							Drop files here or click to upload.
							<br>
							<span class="note needsclick">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>
						</div>
					</form>

					<a href="#" class="btn btn-default" data-tour id="help">Help</a>
					<a href="#" class="btn btn-default" id="getValueButton">Return value</a>
					<div class="dropdown btn btn-default">
						<a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More <span class="caret"></span></a>
						<ul class="dropdown-menu" aria-labelledby="dLabel">
							<li><a href="">Help</a></li>
							<li><a href="">Delete Post</a></li>
						</ul>
					</div>

					<button type="submit" class="btn btn-success pull-right btn-right">Publish</button>
					<a href="#" class="btn btn-primary pull-right btn-right">Save Post</a>
					<a href="#" class="btn btn-default pull-right">Preview Post</a>
				</div>
			</div>
		</div>

		<form method="POST">
			{{ csrf_field() }}

			<!-- Tutorial -->

			<div class="col-md-8">

				<div id="TextBoxesGroup">
				<!-- Textboxes -->
					
					<div id="TextBoxDiv1" class="card stepbox">
						<div>
							<h3>Stap 1</h3>
						</div>
						<div class="form-group">
							<label for="step_title" class="control-label">Titel</label>
							<div>
								<input type="text" class="form-control" id="step_title_1" name="step_title" value="" placeholder="Bijv. benodigdheden">
							</div>
						</div>
						<div class="form-group">
							<label for="step_img" class="control-label">Afbeeldingen</label>
							<div class="dropzone step-dropzone">
								<div class="dz-message needsclick">
									Sleep hier de afbeeldingen naar toe die bij deze stap horen.
								</div>
							</div>
						</div>
							{{-- <div class="dz-message needsclick">
								Drop files here or click to upload.
								<br>
								<span class="note needsclick">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>
							</div> --}}
						<label>Omschrijving</label>
						<textarea class="form-control" rows="4" placeholder="Omschrijving" name="textbox1" id="textbox1"></textarea>
					</div>




				
					<div id="TextBoxDiv2" class="post stepbox form-group">
						<label>Stap 2</label>
						<textarea class="form-control" rows="4" placeholder="Omschrijving" name="textbox2" id="textbox2"></textarea>
					</div>
				
					<div id="TextBoxDiv3" class="post stepbox form-group">
						<label>Stap 3</label>
						<textarea class="form-control" rows="4" placeholder="Omschrijving" name="textbox3" id="textbox3"></textarea>
					</div>
				
				<!-- end textboxes -->
				</div>

				<a href="#_" class="btn btn-success" id="add-step">Stap toevoegen</a>

				<!-- Remove step modal -->
				<button type="button" class="btn btn-warning" data-toggle="modal" data-target=".step-modal" id="remove-step">Verwijder stap</button>

				{{-- step modal --}}
				<div class="modal fade step-modal" tabindex="-1" role="dialog" aria-labelledby="">
				 	<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="exampleModalLabel">Weet je zeker dat je de laatste stap wilt verwijderen?</h4>
							</div>
							<div class="modal-footer">
								<a href="#_" class="btn btn-lg btn-warning" data-dismiss="modal">Terug</a>
								<a href="#_" class="btn btn-danger" id="removeButton" data-dismiss="modal">Verwijder</a>
							</div>
						</div>
					</div>
				</div>

				<!-- submit button -->
				<button type="submit" class="btn btn-success pull-right" id="publish">Publiceer</button>
			</div>

			 <!-- Post info -->
	         <div class="col-md-4">
	            <div class="card post-well">
	            	<h3>Informatie</h3>
	            	<div class="center">
	            		<p>Titel: <strong>x</strong></p>
	            		<p>Aantal stappen: <strong>x</strong></p>
	            	</div>
	                <button type="submit" class="btn btn-lg btn-success" id="publish">Publiceren</button>
                </div>
            </div>
		</form>
	</div>

@endsection

@section('scripts')
	{{-- tour --}}
	<script src="{{ asset('js/bootstrap-tour.js') }}"></script>

	<script type="text/javascript">
		
		$(document).ready(function(){

			//cookies function
			function GetCookie(name) {
		        var arg = name + "=";
		        var alen = arg.length;
		        var clen = document.cookie.length;
		        var i = 0;

		        while (i < clen) {
		            var j = i + alen;
		            
	                if (document.cookie.substring(i,j) == arg)
	                    return "here";
	                	i = document.cookie.indexOf(" ",i) + 1;
	                if (i == 0) 
	                    break;
		        }

		        return null;
		    }

			$(function() {
			    var visit=GetCookie("COOKIE-new-post");

			    if (visit==null){
			    	// intro modal
			        $('#welcome-modal').modal('show');
			    }
			    var expire=new Date();
			    expire=new Date(expire.getTime()+100);
			    document.cookie="COOKIE-new-post=here; expires="+expire;
			});

			//Tour
			var tour = new Tour({
				steps: [
					{
						element: "#title",
						placement: "bottom",
						content: "Hier geef je een passende titel op van je tutorial"
					}, {
						element: "#dropzone",
						placement: "auto",
						content: "Hier upload je een mooie afbeelding van je project"
					}, {
						element: "#textbox1",
						placement: "bottom",
						content: "Hier schrijf je per stap wat je gedaan hebt"
					}, {
						element: "#add-step",
						placement: "top",
						content: "Hier voeg je een extra stap toe"
					}, {
						element: "#remove-step",
						placement: "top",
						content: "Toegevoegde stappen die je niet gebruikt verwijder je hier"
					}, {
						element: "#publish",
						placement: "top",
						content: "Als je klaar bent publiceer je hier je Divvy"
					}, {
						element: "#help",
						placement: "bottom",
						content: "Als je iets vergeet kan je hier opnieuw om hulp vragen"
					}, {
						orphan: true,
						content: "Succes met schrijven!"
					}
				],
			});

			$(document).on("click", "[data-tour]", function(e) {
				e.preventDefault();
				if ($(this).hasClass("disabled")) {
					return;
				}

				tour.restart();
				return $(".alert").alert("close");
			});
		});
		
	</script>

@stop