@extends('main')

@section('title', '| title')

@section('stylesheets')
	<!-- page exclusive styles -->
    <link rel="stylesheet" href="{{ asset('css/posts.css') }}">

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <!-- Post header -->
            <div class="card card-success">
                <div class="post-title">
                    <h2>Wooden airplane<small> Door <a href="">Johan</a> in <a href="">Houtbewerking</a></small></h2>
                </div>
            </div>

            <div class="card">
                <div class="post-cover">
                    <div class="image">
                        <img src="{{ asset('img/post/cover.jpg') }}">
                    </div>
                    <div class="container">
                        <a class="btn cover-btn img-circle pull-right" title="Report this post"><i class="fa fa-flag" aria-hidden="true"></i></a>
                        <a class="btn cover-btn circle pull-right" title="Favourite"><i class="fa fa-heart" aria-hidden="true"></i></a>
                        <a class="btn cover-btn circle pull-right" title="Add to your collection"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <a class="btn cover-btn circle pull-right" title="Share"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                        
                    </div>
                </div>

                <div class="post-navbar">
                    <nav class="navbar nav" id="nav">
                        <form class="form-inline center">
                            <button class="btn btn-primary" type="button">Download</button>
                            <button type="button" class="btn btn-default">Introduction</button>
                            <a class="btn btn-default" href="#step-1">1</a>
                            <button class="btn btn-default" type="button">2</button>
                            <button class="btn btn-default" type="button">3</button>
                            <button class="btn btn-default" type="button">...</button>
                        </form>
                    </nav>
                </div>
                    
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <!-- Post Body -->
            <div class="post-body">

                <!-- Post step (preview)  -->
                <div class="card">
                    <div class="step-header" id="step-1">
                        <div class="">
                            <img src="{{ asset('img/post/step1.jpg') }}">
                        </div>
                    </div>

                    <div class="step-content">
                        <h4>Stap 1: Benodigdheden</h4>
                        <hr>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae justo gravida justo mollis laoreet quis sed quam. Integer justo ex, pellentesque sit amet mattis sit amet, dictum sit amet nisl. Nunc euismod, tellus non sollicitudin venenatis.

                            <ul>
                                <li>Sed facilisis</li>
                                <li>vehicula</li>
                                <li>Aenean</li>
                                <li>maximus nisi</li>
                            </ul>

                            dui volutpat augue, quis tincidunt ipsum nulla eget erat.
                        </p>
                    </div>
                </div>

                <!-- Post step (preview)  -->
                <div class="card">
                    <div class="step-header">
                        <div class="">
                            <img src="{{ asset('img/post/step2.jpg') }}">
                        </div>
                    </div>

                    <div class="step-content">
                        <h4>Step 2: Materialen</h4>
                        <hr>
                        <p>
                            Sed vel arcu et dui iaculis egestas. Vivamus ac tortor nec lacus malesuada elementum ac vitae erat. Nulla blandit nibh ut laoreet egestas. Sed facilisis ipsum a ante semper, vel maximus nisi elementum. Aenean tincidunt vestibulum purus gravida feugiat. Phasellus vehicula eros non vehicula rhoncus. Phasellus ornare massa at ex faucibus pretium. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </p>
                        <blockquote>
                            Sed facilisis ipsum a ante semper, vel maximus nisi elementum.
                        </blockquote>
                        <p>
                            vehicula eros non vehicula rhoncus. 
                        </p>
                    </div>

                    <!-- Post footer -->
                    <div class="card post-footer">
                        <ul class="pager">
                            <li><a href="#">←  Prev</a></li>
                            <li><a href="#">All Steps</a></li>
                            <li><a href="#">Next  →</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <!-- Post info -->
         <div class="col-md-4">
            <div class="card post-well">
                <div class="center">
                    <h3>Over deze tutorial</h3>
                    <hr>
                    <div class="post-meta">
                        <ul class="list-inline">
                            <li alt="Views"><span class="glyphicon glyphicon-eye-open"></span> 114</li>
                            <li alt="Comments"><span class="glyphicon glyphicon-comment"></span> 4</li>
                            <li alt="Downloads"><span class="glyphicon glyphicon-download-alt"></span> 0</li>
                            <li alt="Likes"><span class="glyphicon glyphicon-heart"></span> 22</li>
                        </ul>
                        <hr>
                    </div>
                    <div class="well-info">
                        <div class="well-left">
                            <p>
                                <b>Gepubliceerd op:</b>
                                <br>
                                <b>Weergaven:</b>
                                <br>
                                <b>Favorieten:&nbsp;</b>
                            </p>
                        </div>
                        <div class="well-right">
                            <p>
                                26 maart 2017
                                <br>
                                8.442
                                <br>
                                50
                            </p>
                        </div>
                    </div>
                     
                     <div class="post-auth">
                        <hr>
                        <h4>Auteur:</h4>
                        <div class="auth-tumb well-left">
                            <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        </div>
                        <div class="auth-body well-right">
                            <h4>Johan Bakker</h4>
                            <a href="" class="btn btn-warning">Volgen</a>
                        </div>
                     </div>   

                    <div class="post-social center">
                        <hr>
                        <h4>Deel op:</h4>
                        <a class="btn btn-social btn-facebook"><i class="fa fa-facebook-square"></i></a>
                        <a class="btn btn-social btn-facebook"><i class="fa fa-twitter-square"></i></a>
                        <a class="btn btn-social btn-facebook"><i class="fa fa-google-plus-square"></i></a>
                        <a class="btn btn-social btn-facebook"><i class="fa fa-pinterest-square"></i></a>
                    </div>

                    <div class="post-related">
                        <hr>
                        <h4>Gerelateerd:</h4>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                    </div>

                    <hr>
                    <div>
                        <button class="btn btn-block" onclick="history.go(-1)">Terug</button>
                    </div>

                    <hr>
                </div>
            </div>
        </div>
    </div>

    <!-- Post comments -->
    <div class="row">
        <div class="col-md-8">
            
                <div class="card post-comments card-success">
                    <h3>Voeg een reactie toe</h3>
                
                    <form>
                        <textarea name="" class="form-control" id="comment-box" placeholder="Schrijf hier een reactie..."></textarea>
                        <button type="submit" class="btn btn-primary pull-right hidden" id="btn-comment">Reageren</button>
                    </form>
                </div>

            <div class="post post-comments">
                <h3>Reacties</h3>
                <hr>
                <!-- comment -->
                <div class="comment">
                    <div class="comment-tumb">
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                    </div>
                    <div class="comment-body">
                        <h4>Johan Bakker <small>2 uur geleden</small></h4>
                        <p>
                            Sed vel arcu et dui iaculis egestas.
                        </p>
                        <a href=""><small>Beantwoorden &nbsp; &bull;</small></a>
                        <a href=""><span class="glyphicon glyphicon-thumbs-up"></span></a>
                        <a href=""><span class="glyphicon glyphicon-thumbs-down"></span></a>
                    </div>
                    <hr>
                </div>
                <!-- ..end comment -->

                <!-- comment -->
                <div class="comment">
                    <div class="comment-tumb">
                        <a href="#"><img src="{{ asset('img/user/avatar.jpg') }}"></a>
                    </div>
                    <div class="comment-body">
                        <h4>Johan Bakker <small>2 uur geleden</small></h4>
                        <p>
                            Phasellus ornare massa at ex faucibus pretium. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </p>
                        <a href=""><small>Beantwoorden &nbsp; &bull;</small></a>
                        <a href=""><span class="glyphicon glyphicon-thumbs-up"></span></a>
                        <a href=""><span class="glyphicon glyphicon-thumbs-down"></span></a>
                    </div>
                    <hr>
                </div>
                <!-- ..end comment -->
                <div class="post-comments-footer">
                    <button class="btn btn-default btn-block">Meer reacties</button>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->

    <script type="text/javascript">
        //fixing second nav to header
        $(function() {
            $('#nav-wrapper').height($("#nav").height());
            
            $('#nav').affix({
                offset: { top: $('#nav').offset().top }
            });
        });
    </script>

    <script type="text/javascript">
        //reactie add delete button
        $("#comment-box").focus(function() {
            $('#btn-comment').fadeIn( 500 ).removeClass("hidden");
        });

        $("#comment-box").focusout(function() {
            $('#btn-comment').addClass("hidden");
        });
    </script>
@stop