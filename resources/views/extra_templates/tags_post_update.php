@foreach ($post->tags as $tag)
			<option selected value="{{ $tag->tag }}">{{ $tag->tag }}</option>
	@endforeach
	@foreach($newTags as $tag)
			<option value="{{ $tag }}">{{ $tag }}</option>
	@endforeach

	public function edit($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();
        $data = $this->dispatch(new PostFormFields($slug));

        return view('posts.edit', $data)->withPost($post);


        // //fetch post from db
        // $post = Post::whereSlug($slug)->firstOrFail();
        // $step_count = $post->step_count;

        // //categories and tags
        // $categories = Category::all();
        // $tags = Tag::all();

        // //ff lelijke tags vergelijking
        // foreach($tags as $value){
        //     $tags1[] = $value['tag'];
        // }

        // foreach($post->tags as $value){
        //     $tags2[] = $value['tag'];
        // }

        // $newTags = array_diff($tags1,$tags2);

        // return view('posts.edit', compact('post', 'step_count', 'categories', 'newTags'));
    }


    <select name="category" id="category" class="demo-default" placeholder="Kies een categorie...">
                        <option value="">Kies een categorie...</option>
                        @foreach ($categories as $category)
                            <option class="option" data-selectable="" value="{{ $category->id }}" 
                                {{ (collect(old('category'))->contains($category->id)) ? 'selected':'' }}
                            >{{ $category->category }}</option> 
                        @endforeach
                    </select>