@extends('layouts.main')

@section('title', '| Home')

@section('stylesheets')
    <!-- page exclusive styles -->
@endsection

@section('cover')
	<!-- Cover -->
<div class="cover">
    <div class="image"></div>
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <h1>Welkom op Divvy!</h1>
                    <h3>Een doe-het-zelf community</h3>
                </div>
                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                    <div class="input-group input-group-lg search-box">
                        <input type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-btn">
                            <button class="btn" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div><!-- /input-group -->
                </div>    
            </div>
        </div>
    </div>
	</div>
@endsection

@section('content')
    <div class="container-fluid">

    	{{-- Sponsored posts --}}
	    <div class="row">
	        <section class="col-md-12">
	        	<h3>Sponsored</h3>
	        	<hr class="home-hr">
	        </section>

	        <div class="col-xs-12 col-sm-6 col-md-3" style="display: none;">
	        	
	        </div>
		</div>

		<div class="row">
		    @foreach ($posts as $post)
	        	<!-- post -->
                @include('posts.partials._post_tumb')
	        @endforeach

	    </div>
        <div class="row">
        	<div class="col-md-12">
        		<a href="#" class="btn btn-large btn-warning pull-right">Bekijk alles <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
        	</div>
        </div>
	            
	    {{-- User posts --}}
	    <div class="row">

            <div class="col-md-12">
	        	<h3>Nieuw</h3>
	        	<hr class="home-hr">
	        </div>

	        @foreach ($posts as $post)
                <!-- post -->
                @include('posts.partials._post_tumb')
            @endforeach
            
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<a href="#" class="btn btn-large btn-warning pull-right">Bekijk alles <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
        	</div>
	    </div>

	    {{-- categorieen --}}
	    <div class="row">
	    	<div class="col-md-12">
	    		<div class="text-center">
	    			<h3>Categorieën</h3>
	    			<hr class="home-hr">
	    			<div class="nav-categories">
	    				{{-- categorie icons --}}
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</div>

@endsection

@section('scripts')
    <!-- page exclusive scripts -->
@stop
