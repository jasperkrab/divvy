@extends('layouts.main')

@section('title', '| Contact')

@section('stylesheets')
	<!-- page exclusive styles -->
	<style type="text/css">
		#message {
		    height: 205px;
		}

		.form-group label {
			font-size: 1.5em;
			font-weight: 400;
		}

	</style>
@endsection

@section('cover')
	<!-- cover image -->
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron">
				<h2></h2>
			</div>
		</div>
	</div> <!-- end of header .row -->
	<div class="row">
		<div class="col-md-12">
			<div class="center">
				<h3 class="">Contactformulier</h3>
				<h4 class="h4-bottom">
					Heeft u vragen, tips of iets heel anders te vertellen? Neem dan gerust contact op via het onderstaande formulier. Wij horen graag van u.
				</h4>
			</div>
		</div>

		<span class="hr"></span>

		<form action="{{ route('contact.sent') }}" method="POST">
			{{ csrf_field() }}
			<div class="col-md-6">
				<div class="form-group">
					<label>Naam:</label>
					<input id="name" type="" name="name" class="form-control" placeholder="Naam" value="{{ old('name') }}">
				</div>
				<div class="form-group">
					<label>E-mail:</label>
					<input id="email" type="" name="email" class="form-control" placeholder="E-mail" value="{{ old('email') }}">
				</div>

				<div class="form-group">
					<label>Onderwerp:</label>
					<input id="subject" type="" name="subject" class="form-control" placeholder="Onderwerp" value="{{ old('subject') }}">
				</div>
			</div>
			<div class="col-md-6">

				<div class="form-group">
					<label>Bericht:</label>
					<textarea id="message" type="" rows="8" name="message" class="form-control" placeholder="Schrijf hier het bericht...">{{ old('message') }}</textarea>
				</div>
				<button type="submit" class="btn btn-success btn-lg pull-right">Verstuur</button>
			</div>
		</form>	
	</div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop