@extends('layouts.main')

@section('title', '| Hoe het werkt')

@section('stylesheets')
	<!-- page exclusive styles -->
@endsection

@section('cover')
	<!-- cover image -->
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron">
				<h2></h2>
			</div>
		</div>
	</div> <!-- end of header .row -->
	<div class="row">
		<div class="col-md-12">
			<div class="center">
				<h3 class="">Hoe het werkt</h3>
				<h4 class="h4-bottom">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.
				</h4>
			</div>
		</div>

		<span class="hr"></span>
	</div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop