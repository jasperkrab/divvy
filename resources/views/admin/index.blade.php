@extends('layouts.main')

@section('title', '| Admin panel')

@section('stylesheets')
    <!-- page exclusive styles -->
@endsection

@section('cover')

@endsection

@section('content')
	    <div class="row">
			<div class="col-md-12">	
				
				<div class="card">
					<div class="card-header">
						<h3>Gebruikers en gebruikersrollen</h3>
					</div>
					<div class="card-body">
					    <table class="table table-striped table-bordered">
					    	<thead>
					    		<th>Naam</th>
					    		<th>E-mail</th>
					    		<th>Admin</th>
					    		<th>Subscriber</th>
					    		<th>User</th>
					    		<th>Actie</th>
					    	</thead>
					    	<tbody>
					    		
						    	@foreach($users as $user)
						    	<tr>
						    		<form action="{{ route('admin.assign') }}" method="post">
						    			{{ csrf_field() }}
							    		<td>{{ $user->first_name . ' ' . $user->last_name }}</td>
							    		<td>{{ $user->email }} <input type="hidden" name="email" value="{{ $user->email }}"></td>
							    		<td><input type="checkbox" name="role_admin" {{ $user->hasRole('admin') ? 'checked' : '' }}></td>
							    		<td><input type="checkbox" name="role_subscriber" {{ $user->hasRole('subscriber') ? 'checked' : '' }}></td>
							    		<td><input type="checkbox" name="role_user" {{ $user->hasRole('user') ? 'checked' : '' }}></td>
						    			<td><button type="submit" class="btn btn-default">Assign Role</button></td>
						    		</form>
					    		</tr>
					    		@endforeach
					    		
					    	</tbody>
					    </table>
			    	
			    		<div class="text-center">
					    	{{ $users->links() }}
					    </div>

			    	</div>
			    </div>

	    	</div>
	    </div>

@endsection

@section('scripts')
    <!-- page exclusive scripts -->
@stop
