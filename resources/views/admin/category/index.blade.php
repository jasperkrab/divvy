@extends('layouts.main')

@section('title', '| Categorieën')

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')
	<!-- content -->
	<div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="card-header card-top">
                    <h3><a href="{{ route('categories.index') }}">Categorieën</a> <small> Lijst</small></h3>
                </div>
                <div class="card-body">
                    <div class="pull-right">
                        <a href="{{ route('categories.create') }}" class="btn btn-success margin-bottom">
                            <i class="fa fa-plus-circle"></i> Categorie toevoegen
                        </a>
                    </div>

                    <table id="categories-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Categorieën</th>
                                <th class="hidden-md">Omschrijving</th>
                                <th data-sortable="false">Acties</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category->category }}</td>
                            <td class="hidden-md">{{ $category->meta_description }}</td>
                            <td>
                                <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-info">
                                    <i class="fa fa-pencil"></i> Aanpassen
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop