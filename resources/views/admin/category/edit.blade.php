@extends('layouts.main')

@section('title', '| Categorieën aanpassen')

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')
	<!-- content -->
	<div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="card-header">
                    <h3><a href="{{ route('tags.index') }}">Categorieën</a> <small>Categorie bewerken</small></h3>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('categories.update', $id) }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="category" class="col-md-4 control-label">Tag</label>

                            <div class="col-md-6">
                                <input id="category" type="text" class="form-control" name="category" value="{{ $category }}" autofocus>
                                @include('errors.partials._formerrors', ['errorName' => 'category'])
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
                            <label for="meta_description" class="col-md-4 control-label">Omschrijving</label>

                            <div class="col-md-6">
                                <textarea id="meta_description" type="text" class="form-control" name="meta_description">{{ $meta_description }}</textarea>
                                @include('errors.partials._formerrors', ['errorName' => 'meta_description'])
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4 margin-bottom">
                                <button type="submit" class="btn btn-success pull-right">
                                	<i class="fa fa-plus-circle"></i>
                                    Update
                                </button>
                                <button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#modal-delete">
                                    <i class="fa fa-times-circle"></i> Delete
                                </button>
                            </div>
                            <div class="col-md-6 col-md-offset-4">
                                <button onclick="goBack()" class="btn btn-default pull-right">Terug</button>
                            </div>
                        </div>
                    </form>
                </div>

                {{-- confirm delete --}}
                <div class="modal fade" id="modal-delete" tabIndex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">×</button>
                                <h4 class="modal-title">Categorie verwijderen</h4>
                            </div>
                            <div class="modal-body">
                                <p class="lead">
                                    <i class="fa fa-question-circle fa-lg"></i>  
                                    Weet je zeker dat je deze categorie wil verwijderen?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <form method="POST" action="{{ route('categories.destroy', $id) }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="button" class="btn btn-default"
                                    data-dismiss="modal">Nee</button>
                                    <button type="submit" class="btn btn-danger">
                                        <i class="fa fa-times-circle"></i> Ja
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop