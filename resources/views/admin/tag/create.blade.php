@extends('layouts.main')

@section('title', '| Tag aanmaken')

@section('stylesheets')
	<!-- page exclusive styles -->
@endsection

@section('content')
	<!-- content -->
	<div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="card-header">
                    <h3><a href="{{ route('tags.index') }}">Tags</a> <small>Nieuwe tag toevoegen</small></h3>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('tags.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('tag') ? ' has-error' : '' }}">
                            <label for="tag" class="col-md-4 control-label">Tag</label>

                            <div class="col-md-6">
                                <input id="tag" type="text" class="form-control" name="tag" value="{{ old('tag') }}" autofocus>
                                @include('errors.partials._formerrors', ['errorName' => 'tag'])
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
                            <label for="meta_description" class="col-md-4 control-label">Omschrijving</label>

                            <div class="col-md-6">
                                <textarea id="meta_description" type="text" class="form-control" name="meta_description">{{ old('meta_description') }}</textarea>
                                @include('errors.partials._formerrors', ['errorName' => 'meta_description'])
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success pull-right btn-right">
                                	<i class="fa fa-plus-circle"></i>
                                    Voeg toe
                                </button>
                                <button onclick="goBack()" class="btn btn-default pull-right">Terug</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop