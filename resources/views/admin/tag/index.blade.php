@extends('layouts.main')

@section('title', '| Tags')

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')
	<!-- content -->
	<div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="card-header card-top">
                    <h3><a href="{{ route('tags.index') }}">Tags</a> <small> Lijst</small></h3>
                </div>
                <div class="card-body">
                    <div class="pull-right">
                        <a href="{{ route('tags.create') }}" class="btn btn-success margin-bottom">
                            <i class="fa fa-plus-circle"></i> Tag toevoegen
                        </a>
                    </div>

                    <table id="tags-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Tag</th>
                                <th class="hidden-md">Omschrijving</th>
                                <th data-sortable="false">Acties</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($tags as $tag)
                        <tr>
                            <td>{{ $tag->tag }}</td>
                            <td class="hidden-md">{{ $tag->meta_description }}</td>
                            <td>
                                <a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-info">
                                    <i class="fa fa-pencil"></i> Aanpassen
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->
{{--       <script>
    $(function() {
      $("#tags-table").DataTable({
      });
    });
  </script> --}}
@stop