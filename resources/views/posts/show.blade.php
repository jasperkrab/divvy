@extends('layouts.main')

@section('title', '| title')

@section('stylesheets')
	<!-- page exclusive styles -->
    <link rel="stylesheet" href="{{ asset('css/posts.css') }}">

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <!-- Post header -->
            <div class="card card-success">
                <div class="post-title">
                    <h2>{{ $post->title }}<small> Door <a href="">{{ucfirst($post->user->username)}}</a> in <a href="">Houtbewerking</a></small></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="post-cover">
                    <div class="image">
                        <img src="{{ asset('img/post/cover.jpg') }}">
                    </div>

                </div>
                    
            </div>

            <!-- Post Body -->
            <div class="post-body">

                <!-- Post step (preview)  -->
                    <div class="card">
                        <div class="step-header" id="step-1">
                            <div class="">
                                <img src="{{ asset('img/post/step1.jpg') }}">
                            </div>
                        </div>

                        <div class="step-content">
                            <h4>Stap 1: Benodigdheden</h4>
                            <hr>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae justo gravida justo mollis laoreet quis sed quam. Integer justo ex, pellentesque sit amet mattis sit amet, dictum sit amet nisl. Nunc euismod, tellus non sollicitudin venenatis.

                                <ul>
                                    <li>Sed facilisis</li>
                                    <li>Aenean</li>
                                    <li>maximus nisi</li>
                                </ul>

                                dui volutpat augue, quis tincidunt ipsum nulla eget erat.
                            </p>
                        </div>
                    </div>

            </div>
        </div>

        <!-- Post info -->
        <div class="col-md-4">
            <div class="card post-well">
                <div class="center">
                    <h3>Menu</h3>
                    <hr>
                    <div class="post-meta">
                        <hr>
                    </div>
                    <div class="well-info">
                        <a class="btn btn-success" href="{{ route('posts.edit', $post->slug) }}"><i class="fa fa-pencil"></i> Aanpassen</a>
                        <a class="btn btn-info" href="{{ route('blog.single', $post->slug) }}"><i class="fa fa-eye"></i> Bekijken</a>
                        <hr>
               
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">
                            <i class="fa fa-times-circle"></i> Verwijderen
                        </button>


                        {{-- confirm delete --}}
                        <div class="modal fade" id="modal-delete" tabIndex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                        <h4 class="modal-title">Post verwijderen</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p class="lead">
                                            Weet je zeker dat je deze post wilt verwijderen?
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <form method="POST" action="{{ route('posts.destroy', $post->id) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="button" class="btn btn-default"
                                            data-dismiss="modal">Nee</button>
                                            <button type="submit" class="btn btn-danger">
                                                <i class="fa fa-times-circle"></i> Ja
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div>
                            <button class="btn btn-block" onclick="goBack()">Terug</button>
                        </div>

                        <hr>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop