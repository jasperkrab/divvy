@extends('layouts.main')

@section('title', "| $user->username posts")

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')
	<!-- content -->
    <div class="container-fluid">
        <div class="row">

            @foreach ($user->posts as $post)
                @include('posts.partials._post_tumb')
            @endforeach

            <article class="col-xs-6 col-sm-4 col-md-15">
                <div class="post-tumb-con hover-shadow card-success">
                    <div class="btn-add-post">
                        <a href="#"   data-toggle="modal" data-target="#create-post-modal">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <figcaption class="post-desc">
                                <span class="title">Plaats een nieuwe post</span>
                            </figcaption>
                        </a>
                    </div>
                </div>
            </article>

            <hr>
            <div class="col-md-12">
                <div class="text-center">
                    <!-- post nav -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop