@extends('layouts.main')

@section('title', '| Create new post')

@section('stylesheets')
	<!-- page exclusive styles -->
	<link rel="stylesheet" href="{{ asset('css/posts.css') }}">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-tour.css') }}">
	<link rel="stylesheet" href="{{ asset('css/selectize.css') }}">

	<!--standaard jquery staat in head-->
	
	<!--script voor drag and drop functies-->
	<script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>
	
	<!--script voor de add and remove functie-->
	<script src="{{ asset('js/posts.js') }}"></script>
@endsection

@section('content')

	{{-- Modal --}}
	<div class="modal out fade" id="welcome-modal">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">Leuk dat je een Divvy gaat maken!</h4>
				</div>
				<div class="modal-footer center">
					<a href="" class="btn btn-success" data-dismiss="modal" data-tour>Hoe werkt het?</a>
					<a href="" class="btn btn-danger" data-dismiss="modal">Begrepen!</a>
				</div>
			</div>
		</div>
	</div>

	<form class="" role="form" method="POST" action="{{ route('posts.store') }}" id="form-add-post" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="row">
			<div class="col-md-12">

		<!-- 		<div class="card">
					<div class="card-header card-top card-success">
						{{-- title --}}
						<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">					
							<label for="title"><h3>Titel:</h3></label>
								<input type="text" class="form-control input-lg" id="title" name="title" value="{{ $title }}" placeholder="Geef je project hier een titel">
								
								@if ($errors->has('title'))
				                    <span class="help-block">
				                        <strong style="color: #fff !important;">{{ $errors->first('title') }}</strong>
				                    </span>
				                @endif
						</div>
					</div>
				</div> -->

				{{-- dropzone --}}
				<div id="">
					<div class="post feat-img-box">
						<div class="dropzone">
							<div class="dz-message needsclick">
								Drop files here or click to upload.
								<br>
								<span class="note needsclick">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>
							</div>
						</div>

						<a href="#" class="btn btn-default" data-tour id="help">Help</a>
						<a href="#" class="btn btn-default" id="getValueButton">Return value</a>
						<div class="dropdown btn btn-default">
							<a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More <span class="caret"></span></a>
							<ul class="dropdown-menu" aria-labelledby="dLabel">
								<li><a href="">Help</a></li>
								<li><a href="">Delete Post</a></li>
							</ul>
						</div>

						<button type="submit" class="btn btn-success pull-right btn-right">Publiceren <i class="fa fa-spinner faa-spin animated hide" aria-hidden="true"></i></button>
						<a href="#" class="btn btn-warning pull-right btn-right" id="save-btn">Opslaan <i class="fa fa-spinner faa-spin animated" style="display: none;" aria-hidden="true"></i></a>
						<a href="#" class="btn btn-default pull-right">Voorvertoning</a>
					</div>
				</div>
			</div>

			<!-- Tutorial -->

			<div class="col-md-8">

				<div id="TextBoxesGroup">
				<!-- Textboxes -->
					
				@for ($i = 0; $i < 3; $i++)

					<div id="step-box{{ $i }}" class="card stepbox">
						<div class="step-box-title">
							<h3>Stap {{ $i + 1 }}</h3>
							<hr>
						</div>
						<div class="form-group{{ $errors->has('step-title.*') ? ' has-error' : '' }}">
							<label for="step-title{{ $i }}" class="control-label">Titel</label>
							<div>
								<input type="text" class="form-control" id="step-title{{ $i }}" name="step-title[]" value="{{ old('step-title.'.$i) }}" placeholder="Bijv. benodigdheden">
								@include('errors.partials._formerrors', ['errorName' => 'step-title.*'])

							</div>
						</div>
						
						<div class="form-group{{ $errors->has('step-body.*') ? ' has-error' : '' }}">
							<label for="step-body{{ $i }}" class="control-label">Omschrijving</label>
							<textarea class="form-control" rows="4" placeholder="Beschrijf hier de stap..." name="step-body[]" id="step-body{{ $i }}">{{ old('step-body.'.$i) }}</textarea>
							@include('errors.partials._formerrors', ['errorName' => 'step-body.*'])

						</div>
					</div>

				@endfor
				
					{{-- <div id="TextBoxDiv2" class="post stepbox form-group">
						<label>Stap 2</label>
						<textarea class="form-control" rows="4" placeholder="Beschrijf hier de stap..." name="textbox2" id="textbox2"></textarea>
					</div>
				
					<div id="TextBoxDiv3" class="post stepbox form-group">
						<label>Stap 3</label>
						<textarea class="form-control" rows="4" placeholder="Beschrijf hier de stap..." name="textbox3" id="textbox3"></textarea>
					</div> --}}
				
				<!-- end textboxes -->
				</div>

				<div class="margin-bottom">
					<a href="#step-box{{ $i }}" class="btn btn-success" id="add-step">Stap toevoegen</a>
					<!-- Remove step modal -->
					<button type="button" class="btn btn-warning" data-toggle="modal" data-target=".step-modal" id="remove-step">Verwijder stap</button>
				</div>

				{{-- step modal --}}
				<div class="modal fade step-modal" tabindex="-1" role="dialog" aria-labelledby="">
				 	<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Weet je zeker dat je de laatste stap wilt verwijderen?</h4>
							</div>
							<div class="modal-footer">
								<a href="#_" class="btn btn-lg btn-warning" data-dismiss="modal">Terug</a>
								<a href="#_" class="btn btn-danger" id="removeButton" data-dismiss="modal">Verwijder</a>
							</div>
						</div>
					</div>
				</div>

				<!-- submit button -->
				{{-- <button type="submit" class="btn btn-warning pull-right btn-right">Draft opslaan</button>
				<button type="submit" class="btn btn-success pull-right">Publiceer</button> --}}
			</div>

			 <!-- Post info -->
	         <div class="col-md-4">
	            <div class="card post-well">
	            	 <div class="">
		            	<h3>Informatie</h3>
		            	<hr>
	            		{{-- categories --}}
	            		<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
							<label for="category">Categorie:</label>
							<select name="category" id="category" class="demo-default" placeholder="Kies een categorie...">
								<option value="">Kies een categorie...</option>
								@foreach ($categories as $category)
									<option class="option" data-selectable="" value="{{ $category->id }}" 
										{{ (collect(old('category'))->contains($category->id)) ? 'selected':'' }}
									>{{ $category->category }}</option>	
								@endforeach
							</select>

							@include('errors.partials._formerrors', ['errorName' => 'category'])

						</div>

						{{-- tags --}}
		            	<div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
							<label for="tags">Tags:</label>
							<select name="tags[]" id="tags" class="form-control" placeholder="Voeg tags toe..." multiple>
								@foreach ($allTags as $tag)
									<option @if (in_array($tag, $tags)) selected @endif value="{{ $tag }}">{{ $tag }}</option>
								@endforeach
							</select>

							@include('errors.partials._formerrors', ['errorName' => 'tags'])

						</div>
		            	<hr>
		            		<p>Titel: <strong>x</strong></p>
		            		<p>Aantal stappen: <strong>x</strong></p>

		            	<h3>Afbeeldingen</h3>
		            	<hr>
		            	<div class="" style="height: 100px;">
		            		
		            	</div>
		            	<hr>
		            	<!-- submit button -->
		                <input type="submit" name="save" class="btn btn-warning btn-lg pull-right btn-right" id="save" value="Opslaan">
						<input type="submit" name="publish" class="btn btn-success btn-lg" id="save" value="Publiceer">
		            </div>
                </div>
            </div>
		</div>

	</form>

@endsection

@section('scripts')
	{{-- tour --}}
	<script src="{{ asset('js/bootstrap-tour.js') }}"></script>
	<script src="{{ asset('js/selectize.js') }}"></script>

	<script type="text/javascript">
		/*
		 * Selectize
		 */

		//categories
		$('#category').selectize({
			create: false,
			sortField: {
				field: 'text',
				//direction: 'asc'
			},
			dropdownParent: 'body'
		});

		//tags
		$(function() {
			$("#tags").selectize({
				create: true,
				maxItems: 15,
			});
		});
		
		/*
		 * cookies
		 */
		$(document).ready(function(){

			//cookies function
			function GetCookie(name) {
		        var arg = name + "=";
		        var alen = arg.length;
		        var clen = document.cookie.length;
		        var i = 0;

		        while (i < clen) {
		            var j = i + alen;
		            
	                if (document.cookie.substring(i,j) == arg)
	                    return "here";
	                	i = document.cookie.indexOf(" ",i) + 1;
	                if (i == 0) 
	                    break;
		        }

		        return null;
		    }

		    //first poster cookie
			$(function() {
			    var visit=GetCookie("COOKIE-new-post");

			    if (visit==null){
			    	// intro modal
			        $('#welcome-modal').modal('show');
			    }
			    var expire=new Date();
			    expire=new Date(expire.getTime()+1209600000);
			    document.cookie="COOKIE-new-post=here; expires="+expire;
			});

			/*
			 * Walktrough
			 */
			var tour = new Tour({
				steps: [
					{
						element: "#title",
						placement: "bottom",
						content: "Hier geef je een passende titel op van je tutorial"
					}, {
						element: "#dropzone",
						placement: "auto",
						content: "Hier upload je een mooie afbeelding van je project"
					}, {
						element: "#textbox1",
						placement: "bottom",
						content: "Hier schrijf je per stap wat je gedaan hebt"
					}, {
						element: "#add-step",
						placement: "top",
						content: "Hier voeg je een extra stap toe"
					}, {
						element: "#remove-step",
						placement: "top",
						content: "Toegevoegde stappen die je niet gebruikt verwijder je hier"
					}, {
						element: "#publish",
						placement: "top",
						content: "Als je klaar bent publiceer je hier je Divvy"
					}, {
						element: "#help",
						placement: "bottom",
						content: "Als je iets vergeet kan je hier opnieuw om hulp vragen"
					}, {
						orphan: true,
						content: "Succes met schrijven!"
					}
				],
			});

			$(document).on("click", "[data-tour]", function(e) {
				e.preventDefault();
				if ($(this).hasClass("disabled")) {
					return;
				}

				tour.restart();
				return $(".alert").alert("close");
			});
		});
		
	</script>

@stop