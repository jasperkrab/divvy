<a href="#" data-toggle="modal" data-target="#create-post-modal">
	<article class="col-xs-6 col-sm-4 col-md-15">
        <div class="post-tumb-con hover-shadow card-success">
            <div class="post-image">
            	<img src="{{ asset('img/newpost.jpg') }}" class="img-responsive">
            </div>
            <figcaption class="post-desc add-post-text">
	            <hr>
	            <h4>Plaats een nieuwe post</h4>
	        </figcaption>
        </div>
    </article>
</a>