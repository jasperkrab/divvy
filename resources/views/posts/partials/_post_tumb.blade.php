<!-- post -->
<article class="col-xs-6 col-sm-4 col-md-15">
    <div class="post-tumb-con hover-shadow">
        <div class="post-image">
            <a href="{{ route('blog.single', $post->slug) }}">
                @if(file_exists('storage/posts/'. $post->id .'/img/' . $post->post_cover))
                    <img src="{{ asset('storage/posts/'. $post->id .'/img/' . $post->post_cover) }}" class="img-responsive">
                @else
                    <img src="{{ asset('storage/posts/default/img/tumb.jpg') }}" class="img-responsive">
                @endif
            </a>
        </div>
        <figcaption class="post-desc" >
            <span class="title"><a href="{{ route('blog.single', $post->slug) }}">{{ $post->title }}</a></span>
            <hr>
            <span class="author">Door <a href="{{ route('profile', $post->user->username) }}">{{ucfirst($post->user->username)}}</a></span>
            <span class="category">in <a href="{{ route('category.index', $post->category) }}">{{ $post->category->category }}</a></span>
        </figcaption>
        <div class="post-meta" style="display: none;">
            <ul class="list-inline">
                <li alt="Views"><span class="glyphicon glyphicon-eye-open"></span> 114</li>
                <li alt="Comments"><span class="glyphicon glyphicon-comment"></span> 4</li>
                <li alt="Downloads"><span class="glyphicon glyphicon-download-alt"></span> 0</li>
                <li alt="Likes"><span class="glyphicon glyphicon-heart"></span> 22</li>
            </ul>
        </div>
    </div>
</article>