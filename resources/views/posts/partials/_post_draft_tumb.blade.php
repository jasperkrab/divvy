<!-- post -->
<article class="col-xs-6 col-sm-4 col-md-15">
    <div class="post-tumb-con hover-shadow">
        <div class="post-image">
            <a href="">
                @if(file_exists('storage/posts/'. $post->id .'/img/' . $post->post_cover))
                    <img src="{{ asset('storage/posts/'. $post->id .'/img/' . $post->post_cover) }}" class="img-responsive">
                @else
                    <img src="{{ asset('storage/posts/default/img/tumb.jpg') }}" class="img-responsive">
                @endif
            </a>
        </div>
        <figcaption class="post-desc">
            <span class="title">{{ $post->title }}</span>
            <hr>
            <button class="btn btn-danger btn-sm delete-post" data-toggle="modal" data-target="#delete-post-modal" data-remove_url="{{ route('posts.destroy', $post->slug) }}">Verwijderen</button>
            <a href="{{ route('posts.edit', $post->slug) }}" class="btn btn-warning btn-sm">Bewerken</a>
        </figcaption>
    </div>
</article>