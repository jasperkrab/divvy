@extends('layouts.main')

@section('title', '| Create new post')

@section('stylesheets')
	<!-- page exclusive styles -->
	<link rel="stylesheet" href="{{ asset('css/posts.css') }}">
	<link rel="stylesheet" href="{{ asset('css/selectize.css') }}">

	<!--standaard jquery staat in head-->
	
	<!--script voor drag and drop functies-->
	<script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>
	
	<!--script voor de add and remove functie-->

@endsection

@section('content')

	<div id="ajaxResponse"></div>

	<div class="row">

		<form class="" role="form" method="POST" action="{{ route('posts.update', $slug) }}" id="form-add-post">
			{{ csrf_field() }}
			{{ method_field('PUT') }}

			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-top card-success">
						{{-- title --}}
						<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">					
							<label for="title"><h3>Titel:</h3></label>
								<input type="text" class="form-control input-lg" id="title" name="title" required="true" value="{{ old('title', $post->title) }}" placeholder="Geef je project hier een titel">
								
								@if ($errors->has('title'))
				                    <span class="help-block">
				                        <strong style="color: #fff !important;">{{ $errors->first('title') }}</strong>
				                    </span>
				                @endif
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-8">
				<div id="step-box-group">

					<!-- old stepboxes -->
					@foreach($post->steps as $i => $step)
						{{-- old steps --}}
						<div id="step-box-{{ $i }}" class="card step-box">
							<div class="card-header step-box-title">
								<div class="pull-right">
									<a type="button" class="btn btn-sm btn-danger delete-step" data-toggle="modal" data-step_nr="step-box-{{ $i }}" data-step_id="{{ $step->id }}" data-target="#delete-step-modal" data-delete_link="{{ route('steps.destroy', $step->id) }}"><i class="fa fa-times-circle"></i> Stap verwijderen</a>
								</div>
								<h4>(old) Stap {{ $step->step_nr }}</h4>
							</div>
							<div class="card-body">
								{{-- step form --}}
								<div class="form-group{{ $errors->has('step-title-.'.$i) ? ' has-error' : '' }}">
									<label for="step-title-{{ $i }}" class="control-label">Titel</label>
									<input type="text" class="form-control" placeholder="Titel" name="step-title-[]" id="step-title-{{ $i }}" value="{{ old('step-title-.'.$i, $step->title) }}">
								</div>
								<div class="form-group{{ $errors->has('step-body-.'.$i) ? ' has-error' : '' }}">
									<label for="step-body-{{ $i }}" class="control-label">Omschrijving</label>
									<textarea class="form-control" rows="4" placeholder="Beschrijf hier de stap..." name="step-body-[]" id="step-body-{{ $i }}">{{ old('step-body-.'.$i, $step->body) }}</textarea>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>

			<!-- Post info -->
			<div class="col-md-4">
				<div class="card post-well">
					<h3>Informatie</h3>
	            	<hr>
	        		{{-- categories --}}
	        		<div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
						<label for="category">Categorie:</label>
						<select name="category" id="category" class="demo-default" placeholder="Kies een categorie...">
							<option value="">Kies een categorie...</option>
							@foreach ($allcategories as $category)
								<option data-selectable="" @if (in_array($category, $categories)) selected @endif value="{{ $category }}">{{ $category }}</option>
							@endforeach
						</select>
						@include('errors.partials._formerrors', ['errorName' => 'category'])

					</div>

					{{-- tags --}}
	            	<div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
						<label for="tags">Tags:</label>
						<select name="tags[]" id="tags" class="form-control" placeholder="Voeg tags toe..." multiple>
							@foreach ($allTags as $tag)
					            <option @if (in_array($tag, $tags)) selected @endif value="{{ $tag }}">{{ $tag }}</option>
					         @endforeach
						</select>
						@include('errors.partials._formerrors', ['errorName' => 'tags'])
					</div>
					<hr>
					<input class="btn btn-warning btn-lg" type="submit" value="Opslaan" name="save" id="form-add-post"></input>
				</div>
			</div>
		</form>

		<!-- </form> -->
		<div class="col-md-8">
			<form class="" role="form" method="POST" action="{{ route('steps.store', $post->slug) }}">
				{{ csrf_field() }}
				<a href="#_" class="btn btn-success" id="add-step">Stap toevoegen</a>
				<!-- <button class="btn btn-success" type="submit" id="add-step">Stap toevoegen</button> -->
			</form>
		</div>

		{{-- delete step modal --}}
		<div class="modal fade" id="delete-step-modal" tabindex="-1" role="dialog" aria-labelledby="">
		 	<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Weet je zeker dat je deze stap wilt verwijderen?</h4>
					</div>
					<div class="modal-footer">
						<form class="" role="form" method="POST" id="remove-step-form" action="">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}

							<button type="button" class="btn btn-lg btn-warning" data-dismiss="modal">Terug</button>
							<input class="btn btn-danger" type="submit" value="Verwijder" name="delete" id="remove-step" data-dismiss="modal"></input>
						</form>
					</div>
				</div>
			</div>
		</div>
	
	</div> <!-- end row -->

@endsection

@section('scripts')
	{{-- tour --}}
	<script src="{{ asset('js/selectize.js') }}"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {

			//global variables
			var counter = {{ $post->step_count }} + 1;
			var step_nr = null;
			var step_remove_url = null;

			//fixed sidebar
			$('.post-well').affix({
				offset: {
				    top: 210,
			  	}
			})

			$("#step-box-group").sortable({
				stop: function (event, ui) {
					var children = $(this).children("div.step-box");
					children.each(function (index) {
						var l = $(this).find(".step-box-title");
						var html = "<h4>(new) Stap " + (index + 1) + "</h4>";
						l.html(html);
					});
				}
			});

			/*_______________________
			 | add steps function    |
			 |______________________*/

		    $("#add-step").click(function () {

				if(counter > 30) {
					alert("Helaas is het niet toegestaan meer dan 30 stappen te gebruiken.");
					return false;
				}

				var newStep = $(document.createElement('div'))
					.attr("id", 'step-box-' + counter)
					.attr("class", 'card step-box'); //class toevoeging

				newStep.after().html('<div class="card-header step-box-title"><h4>(new)&nbsp;Stap&nbsp;'+ counter + '&nbsp;</h4></div>' + '<div class="card-body"><div class="form-group"><label for="step-title-' + counter + '" class="control-label">Title</label><input type="text" class="form-control" placeholder="Titel" id="step-title-' + counter + '" name="step-title-[]"></div>' + '<div class="form-group"><label for="step-body-' + counter + '" class="control-label">Omschrijving</label><textarea class="form-control" rows="4" placeholder="Beschrijf hier de stap..." name="step-body-[]" id="step-body-'+ counter + '"></textarea></div></div></div>');

				newStep.appendTo("#step-box-group"); //voegt de nieuwe div toe aan deze class of id
				
				counter++;
			});

			//pass step id to delete modal
			$('.delete-step').click(function () {
				stepNr = $(this).data('step_nr');

				//step_remove_url = $(this).data('delete_link');
				step_remove_url = '{{ URL::route('steps.destroy', $step->id) }}';

				//console.log(counter);
			});

			//remove step dynamic
		    $("#remove-step").click(function (e) {
				if(counter == 2) {
					alert("Je moet minimaal één stap omschrijven.");
					return false;
				} else {
					//remove dynamic step
					$("#" + stepNr).remove();

					//reorder stepcount
					var children = $("#step-box-group").children("div.step-box");
					children.each(function (index) {
						var l = $(this).find(".step-box-title");
						var html = "<h4>(new) Stap " + (index + 1) + "</h4>";
						l.html(html);
					});

					e.preventDefault();

					//delete post data
					$.ajax({
						type: 'POST',
						url: step_remove_url,
						data: {
							'_token': $('input[name=_token]').val(),
						},
					});

					step_remove_url = null;

					//save new data
					//$( "#form-add-post" ).submit();
				}

				counter--;
			});

			/*________________
			 | selectize      |
			 |________________*/

			//categories
			$('#category').selectize({
				create: false,
				sortField: {
					field: 'text',
					//direction: 'asc'
				},
				dropdownParent: 'body'
			});

			//tags
			$(function() {
				$("#tags").selectize({
					create: true,
					maxItems: 15,
				});
			});

			//submit step
			// $('#add-step').click(function(e){
			// 	e.preventDefault();

			// 	//post data
			// 	$.ajax({
			// 		type: 'POST',
			// 		url: '{{ URL::route('steps.store', $post->slug) }}',
			// 		data: {
			// 			'_token': $('input[name=_token]').val(),
			// 		},
			// 	});
			// });
		
		});
	</script>

@stop