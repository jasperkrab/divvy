@extends('layouts.main')

@section('title', '| Create new post')

@section('stylesheets')
	<!-- page exclusive styles -->
	<link rel="stylesheet" href="{{ asset('css/posts.css') }}">

	<!--standaard jquery staat in head-->
	
	<!--script voor drag and drop functies-->
	<script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script>
	
	<!--script voor de add and remove functie-->
	<script src="{{ asset('js/posts.js') }}"></script>
@endsection

@section('content')

<div class="row">
		<div class="col-md-10">
			<div class="card">
				<div>
					<div class="hovercover center" id="bgimage">
						<img src="{{ asset('storage/posts/default/img/post_cover.jpg') }}" class="img-responsive" style="margin-top: {{ $image->position }};">
	                    <div class="hover-div">
	                       <form method="post" id="hoverform" action="{{url('/upload')}}" enctype="multipart/form-data">
	                       {{ csrf_field() }}
	                            <label for="file-upload" class="custom-file-upload" title="Omslag aanpassen">
	                                <i class="fa fa-file-image-o"></i>&nbsp; Change Cover
	                            </label>

	                            <input id="file-upload" name="file" type="file" />
	                         </form>
	                    </div>
		            </div>
		 
	                <div class="hovercover1 center" id="adjimage" style="display: none; ">
	                   
	                </div>
				</div>
			</div>
		</div>
		
@endsection

@section('scripts')
	{{-- tour --}}
	<script src="{{ asset('js/bootstrap-tour.js') }}"></script>
	<script src="{{ asset('js/selectize.js') }}"></script>

 	<script type="text/javascript" src="{{asset('js/jquery.wallform.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jwincrop.js')}}"></script>

	<script type="text/javascript">

		//cover upload
		// submit the form soon after change image
		$('#file-upload').on('change',function(){

			$("#hoverform").ajaxForm({target: '#adjimage',
				success:function(){
			$(".hover-div").hide();

			$("#postcover").hide();
			$("#adjimage").show();


			}}).submit();
		});

		$('.hovercover').each(function() {
		//set size
		var th = $(this).height(),//box height
			tw = $(this).width(),//box width
			im = $(this).children('img'),//image
			ih = im.height(),//inital image height
			iw = im.width();//initial image width
		if (ih>iw) {//if portrait
			im.addClass('ww').removeClass('wh');//set width 100%
		} else {//if landscape
			im.addClass('wh').removeClass('ww');//set height 100%
		}
		//set offset
		var nh = im.height(),//new image height
			nw = im.width(),//new image width
			hd = (nh-th)/2,//half dif img/box height
			wd = (nw-tw)/2;//half dif img/box width
		if (nh<nw) {//if portrait
			im.css({marginLeft: '-'+wd+'px', marginTop: 0});//offset left
		} else {//if landscape
			im.css({marginTop: '-'+hd+'px', marginLeft: 0});//offset top
		}
		});
	</script>

@stop