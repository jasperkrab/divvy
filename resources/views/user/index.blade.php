@extends('layouts.profile')

@section('title', '| Profiel')

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')

	@include('user.partials._profile_header')
	
	<!-- content -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
	    		@foreach ($posts as $post)
	                @include('posts.partials._post_tumb')
	        	@endforeach
	    	</div>
	    	<div class="col-md-12">
	        	<hr class="home-hr">
	        </div>
	    </div>
	</div>

@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop