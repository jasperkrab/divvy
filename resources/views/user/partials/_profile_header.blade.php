<div class="backdrop">
		<div class="container">
			<div class="row">
				<div class="col-md-6 pull-right">
					<div>
						@if (Auth::check())
							@if(Auth::user()->id == $user->id)
								<div class="">
									<a href="{{ route('account', Auth::user()->username) }}" class="btn btn-lg btn-default margin-bottom pull-right">Profiel aanpassen</a>
								</div>
							@endif
						@endif
					</div>
				</div>
				<div class="col-md-6 pull-left">
					<div class="profile-header">
						<div class="d-inline margin-right">
							<img src="{{ asset('storage/img/avatars/' . $user->avatar) }}" class="avatar">
						</div>
						<div class="d-inline">
							<h2>{{ ucfirst($user->username) }}</h2>
							@if (Auth::check())
								@if(Auth::user()->id == $user->id)
									<a href="" class="btn btn-lg btn-block btn-default"><i class="fa fa-envelope-o" aria-hidden="true"></i> Inbox</a>
								@else
									<a class="btn btn-lg btn-bottom btn-warning"><i class="fa fa-plus" aria-hidden="true"></i> Volgen</a>
									<a class="btn btn-lg btn-block btn-default"><i class="fa fa-envelope-o" aria-hidden="true"></i> Berichten</a>
								@endif
							@endif
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card card-bot-0">
						<div class="center margin-bottom">
							<div class="col-md-4">
								<h3><small>Lid sinds</small> {{ date('d F Y', strtotime($user->created_at)) }}</h3>
							</div>
							<div class="col-md-4">
								<h3>{{ count($user->posts->where('status', 1)) }}<small> Posts</small></h3>
							</div>
							<div class="col-md-4">
								<h3>0<small> Volgers</small></h3>
							</div>
						</div>
						<div class="col-md-6 col-md-offset-3">
							<ul class="nav profile-nav nav-justified">
								<li role="presentation" class="active"><a href=""><h3>Posts</h3></a></li>
								<li role="presentation" class=""><a href=""><h3>Favourites</h3></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>