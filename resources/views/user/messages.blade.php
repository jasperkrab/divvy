@extends('layouts.main')

@section('title', '| Title')

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')
	<div class="back-cover-color">
	</div>

	<div class="row">
		<div class="account-header center">
			<div class="col-md-12">
				<h2 class="h4-bottom">Accountgegevens</h2>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="card">
				<nav class="account-nav">
					<ul class="nav nav-pills nav-stacked">
						<li role="presentation" class=""><a href="" class="btn-block"><h4>Basis gegevens</h4></a></li>
					</ul>
				</nav>
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="panel-heading">
					<h3>Basis gegevens</h3>
                    <hr>
				</div>
				<div class="panel-body">

				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop