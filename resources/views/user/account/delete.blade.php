@extends('layouts.main')

@section('title', '| Wachtwoord vervangen')

@section('stylesheets')
	<!-- page exclusive styles -->
@endsection

@section('content')

	<div class="back-cover-color">
	</div>

	<div class="row">
		<div class="account-header center">
			<div class="col-md-12">
				<h2 class="h4-bottom">Accountgegevens</h2>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				@include('user.account.partials._accountmenu')
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="panel-heading">
					<h3>Acccount opzeggen</h3>
                    <hr>
				</div>
				<div class="panel-body">
                <h4 class="h4-bottom">
                    Je bent vrij om je account te verwijderen. Dit kan alleen niet meer ongedaan gemaakt worden.
                </h4>

					<form class="form-horizontal" role="form" method="POST" action="{{ route('account.destroy') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".remove-account-modal">Account verwijderen</button>

                                <!-- delete account modal -->
                                <div class="modal fade remove-account-modal" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel">Je staat op het punt om je account en al je gegevens te verwijderen. Weet je dit zeker?</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="#" class="btn btn-lg btn-warning" data-dismiss="modal">Terug</a>
                                                <button class="btn btn-danger" type="submit">Verwijder mijn account</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop