@extends('layouts.main')

@section('title', '| Account')

@section('stylesheets')
	<!-- page exclusive styles -->
@endsection

@section('content')

	<div class="back-cover-color">
	</div>

	<div class="row">
		<div class="account-header center">
			<div class="col-md-12">
				<h2 class="h4-bottom">Accountgegevens</h2>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="card">
				@include('user.account.partials._accountmenu')
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="panel-heading">
					<h3>Basis gegevens</h3>
                    <hr>
				</div>
				<div class="panel-body">

					<form class="form-horizontal" id="upload" role="form" method="POST" action="{{ route('account.update') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        	<label for="first_name" class="col-md-4 control-label">Voornaam</label>
                        	<div class="col-md-6">
	                        	<input type="text" class="form-control" value="{{ $user->first_name }}" name="first_name">
                                @include('errors.partials._formerrors', ['errorName' => 'first_name'])
                        	</div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        	<label for="last_name" class="col-md-4 control-label">Achternaam</label>
                        	<div class="col-md-6">
	                        	<input type="text" class="form-control" value="{{ $user->last_name }}" name="last_name">
                                @include('errors.partials._formerrors', ['errorName' => 'last_name'])
                        	</div>
                        </div>

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Gebruikersnaam</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{  $user->username }}">
                                @include('errors.partials._formerrors', ['errorName' => 'username'])
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{  $user->email }}">
                                @include('errors.partials._formerrors', ['errorName' => 'email'])
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                            <label for="location" class="col-md-4 control-label">Woonplaats</label>
                            <div class="col-md-6">
                                <input id="location" type="text" class="form-control" name="location" value="{{  $user->location }}">
                                @include('errors.partials._formerrors', ['errorName' => 'location'])
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
                            <label for="avatar" class="col-md-4 control-label">Avatar</label>
                            <div class="col-md-6">
                                <div class="prev-avatar">
                                   <img src="{{ asset('storage/img/avatars/' . $user->avatar) }}" />
                                </div>
                                {{-- <input id="avatar" type="file" class="form-control" name="avatar" value=""> --}}

                                <div class="box">
                                    <input type="file" name="avatar" id="avatar" class="inputfile inputfile-6" multiple />
                                    <label for="avatar"><span class="inputfile-span"></span>&nbsp;Afbeelding kiezen</label>
                                </div>
                                @include('errors.partials._formerrors', ['errorName' => 'avatar'])
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-danger pull-right">Update</button>
                            </div>
                        </div>
                    </form>

				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
	<!-- page exclusive scripts -->
    {{-- $('#select-country').selectize(); --}}
    <script src="{{ asset('js/jquery.custom-file-input.js') }}"></script>
@stop