
<nav class="account-nav">
	<ul class="nav nav-pills nav-stacked">
		<li role="presentation" class="{{ Request::is('account') ? "active" : '' }}"><a href="{{ route('account') }}" class="btn-block"><h4>Basis gegevens</h4></a></li>
		<li role="presentation" class="{{ Request::is('account/wachtwoord') ? "active" : '' }}"><a href="{{ route('account.password') }}" class="btn-block"><h4>Wachtwoord wijzigen</h4></a></li>
		<li role="presentation" class="disabled"><a href="" class="btn-block disabled"><h4>Lidmaatschap</h4></a></li>
        <li role="presentation" class="disabled"><a href="" class="btn-block disabled"><h4>Linked accounts</h4></a></li>
        <li role="presentation" class="{{ Request::is('account/account_verwijderen') ? "active" : '' }}"><a href="{{ route('delete.account') }}" class="btn-block"><h4>Account opzeggen</h4></a></li>
	</ul>
</nav>