@extends('layouts.main')

@section('title', '| Wachtwoord vervangen')

@section('stylesheets')
	<!-- page exclusive styles -->
@endsection

@section('content')

	<div class="back-cover-color">
	</div>

	<div class="row">
		<div class="account-header center">
			<div class="col-md-12">
				<h2 class="h4-bottom">Accountgegevens</h2>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				@include('user.account.partials._accountmenu')
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="panel-heading">
					<h3>Wachtwoord wijzigen</h3>
                    <hr>
				</div>
				<div class="panel-body">

					<form class="form-horizontal" id="upload" role="form" method="POST" action="{{ route('password.update', Auth::user()) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                        	<label for="old_password" class="col-md-4 control-label">Oud Wachtwoord</label>
                        	<div class="col-md-6">
	                        	<input type="password" class="form-control" placeholder="Oud wachtwoord" value="{{ old('old_password') }}" name="old_password">
                                @include('errors.partials._formerrors', ['errorName' => 'old_password'])
                        	</div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Nieuw Wachtwoord</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" value="" placeholder="Nieuw wachtwoord" name="password">
                                @include('errors.partials._formerrors', ['errorName' => 'password'])
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Bevestiging</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" value="" placeholder="wachtwoord bevestigen" name="password_confirmation">
                                @include('errors.partials._formerrors', ['errorName' => 'password_confirmation'])
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-danger pull-right">Update</button>
                            </div>
                        </div>
                    </form>

				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
	<!-- page exclusive scripts -->
@stop