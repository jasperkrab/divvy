@extends('layouts.profile')

@section('title', '| Profiel')

@section('stylesheets')
	<!-- page exclusive styles -->

@endsection

@section('content')

	@include('user.partials._profile_header')
	
	<!-- content -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
	    		@foreach ($posts as $post)
	                @include('posts.partials._post_draft_tumb')
	        	@endforeach
	    	
	    		@include('posts.partials._post_new_tumb')

	    	</div>
	    	<div class="col-md-12">
	        	<hr class="home-hr">
	        </div>
	    </div>

	    {{-- delete step modal --}}
		<div class="modal fade" id="delete-post-modal" tabindex="-1" role="dialog" aria-labelledby="">
		 	<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Weet je zeker dat je deze post wilt verwijderen?</h4>
					</div>
					<div class="modal-footer">
						<form class="" role="form" method="POST" id="remove-post-form" action="">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}

							<a type="button" class="btn btn-lg btn-warning" data-dismiss="modal">Terug</a>
							<a class="btn btn-danger" type="submit" data-dismiss="modal">Verwijder</a>
						</form>
					</div>
				</div>
			</div>
		</div>


	</div>

@endsection

@section('scripts')
	<script type="text/javascript">
	<!-- page exclusive scripts -->
		$(document).ready(function() {

			//pass post id to delete modal
			$('.delete-post').click(function () {
				post_remove_url = $(this).data('remove_url');
				$("#remove-post-form").attr('action', post_remove_url);
			});

		});
	</script>
@stop