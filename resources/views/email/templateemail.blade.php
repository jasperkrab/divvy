<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://xxx.x0.---/XX/xxxx0/xxxxx.---">
<html>
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <link type="image/x-icon" rel="shortcut icon" href="http://d672eyudr6aq1.cloudfront.net/img/email/shared_email_images/sprout-favicon.png">
    <title>E-mail template</title>
    <style
    type="text/css">/* Client-specific Styles */ #outlook a{padding:0;} /* Force Outlook to provide
      a "view in browser" button. */ body{width:100% !important;} /* Force Hotmail to
      display emails at full width */ body{-webkit-text-size-adjust:none;} /* Prevent
      Webkit platforms from changing default text sizes. */ /* Reset Styles */ body{margin:0;
      padding:0;} img{border:none; font-size:14px; font-weight:bold; height:auto; line-height:100%;
      outline:none; text-decoration:none; text-transform:capitalize;} #backgroundTable{height:100%
      !important; margin:0; padding:0; width:100% !important;}</style>
  </head>
  
  <body style="background-color:#e8e8e8; color:#535353; font-size:13pt; font-family:Helvetica, Arial, sans-serif; line-height:1.4; width:100% !important; -webkit-text-size-adjust:none; margin:0; padding:0; "
  yahoo="ignores_media_queries_and_adds_contents_to_styles" width="100%">
    <div>
      <div class="mktEditable">
        <!--Pre-Header Text=-->
        <p><span style="display: none; color: #e8e8e8; font-size: 1px;">Preview panel text</span>
        </p>
        <!--Email Container-->
        <table id="container" style="background: #e8e8e8; line-height: 1.4em;" border="0"
        cellspacing="0" cellpadding="0" width="98%" align="center">
          <tbody>
            <!--Email Header-->
            <tr>
              <td>
                <table style="background-color: #e8e8e8;" border="0" cellspacing="0" cellpadding="0"
                width="600" align="center">
                  <tbody>
                    <tr width="600" height="65">
                      <td style="padding-bottom: 15px;" width="202" valign="bottom">
                        <a style="color: #7ac143;" title="Visit sproutsocial.com" href="" target="_blank">
                          <img style="" src="" alt="Divvy Logo" width="170" height="29" />
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <!--end Email Header-->
            <!--Email Body-->
            <tr>
              <td>
                <table id="body" style="background-color: #ffffff;" border="0" cellspacing="0"
                cellpadding="0" width="600" align="center">
                  <tbody>
                    <!--Email Header-->
                    <tr>
                      <td>
                        <!--Header Image-->
                        <table style="background-color: #fff;" border="0" cellspacing="0" cellpadding="0"
                        width="600" align="center">
                          <tbody>
                            <tr style="background-color: #fff;" width="600">
                              <td style="text-align: center;" width="600" height="100">
                                  <img style="" src="http://d672eyudr6aq1.cloudfront.net/img/email/webinar_advanced/email-webinar-advanced-header-4.png" border="0" alt="" width="600" height="426"
                                  />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <!--end Header Image-->
                      </td>
                    </tr>
                    <!--end Email Header-->
                    <!--Email Sections-->
                    <tr>
                      <td>
                        <!--Intro Paragraph-->
                        <table style="background-color: #ffffff; line-height: 1.6em; color: #808080; font-size: 16px;"
                        border="0" cellspacing="0" cellpadding="0" width="600" align="center">
                          <tbody>
                            <tr width="600">
                              <td style="padding-left: 40px; padding-right: 40px; padding-top: 40px; text-align: left;"
                              width="520">
                                <p style="color: #4d4d4d; font-weight: 300; line-height: 24px; font-size: 16px; margin-bottom: 20px; font-family: Helvetica, Arial, sans-serif; text-align: center; padding: 0;">
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dolor nulla, auctor non euismod vitae, mattis sed justo. Class aptent taciti sociosqu ad litora torquent per conubia nostra
                                </p>
                              </td>
                            </tr>
                            <tr width="600">
                              <td style="padding-left: 40px; padding-right: 40px; padding-top: 40px; text-align: left;"
                              width="520">
                                
                                <h2 style="color: #333; font-weight: 400 !important; line-height: 34px; font-size: 28px; margin-top: 0; margin-bottom: 0; font-family: Helvetica, Arial, sans-serif; text-align: center; padding: 0;">Bedankt voor het aanmelden
                                </h2>

                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <!--end Intro Paragraph-->
                        <!--Features-->
                        <table style="background-color: #ffffff; line-height: 1.6em; color: #808080; font-size: 16px;"
                        border="0" cellspacing="0" cellpadding="0" width="600" align="center">
                          <tbody>                           
                            <tr width="600">
                              <td style="padding-left: 40px; padding-right: 40px; padding-top: 60px; padding-bottom: 60px; text-align: center;"
                              colspan="2" width="520" height="30">
                                <a style="color: #7ac143; font-weight: 400; font-family: Helvetica, Arial, sans-serif;"
                                title="Aanmelden" href=""
                                target="_blank">
                                  <img style="font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: color; text-transform: uppercase; letter-spacing: 1px;"
                                  src="http://d672eyudr6aq1.cloudfront.net/img/email/webinar_advanced/email-webinar-advanced-signup-button.png"
                                  alt="Aanmelden" width="276" height="30" />
                                </a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <!--end Features-->
                        <hr style="border-top: 1px solid #ccc; border-right: 0; border-bottom: 0; border-left: 0; width: 520px; margin: 0;"
                        />
                        <!--Closing Section-->
                        <table style="background-color: #ffffff; line-height: 1.6em; color: #535353; font-size: 16px;"
                        border="0" cellspacing="0" cellpadding="0" width="600" align="center">
                          <tbody>
                            <tr width="600">
                              <td style="padding-left: 40px; padding-right: 40px; padding-top: 30px; padding-bottom: 50px;"
                              colspan="3" width="520">
                                <p style="color: #4d4d4d; font-weight: 300; line-height: 24px; font-size: 16px; margin-top: 0; margin-bottom: 20px; font-family: Helvetica, Arial, sans-serif; padding: 0;">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dolor nulla, auctor non euismod vitae, mattis sed justo.
                                  </p>
                                <div style="color: #4d4d4d; font-weight: 300; line-height: 24px; font-size: 16px; margin-top: 30px; margin-bottom: 0; font-family: Helvetica, Arial, sans-serif; padding: 0;">
                                    Jasper Krab
                                </div>
                                <div style="color: #4d4d4d; font-weight: 300; line-height: 24px; font-size: 14px; margin-top: 0; margin-bottom: 0; font-family: Helvetica, Arial, sans-serif; padding: 0;"><em>Het Divvy team</em>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <!--end closing section-->
                        <!--Contact Information-->
                        <table style="background-color: #ffffff; line-height: 1.6em; color: #535353; font-size: 16px;"
                        border="0" cellspacing="0" cellpadding="0" width="600" align="center">
                          <tbody>
                            <tr width="600">
                              <td style="text-align: center; padding-left: 40px; padding-bottom: 20px;" width="220">
                                <p style="color: #bbbbbb !important; font-weight: 400; line-height: 20px; font-size: 12px; margin-top: 0px; margin-bottom: 0px; padding-top: 0; padding-bottom: 0; font-family: Helvetica, Arial, sans-serif;">
                                  <a style="color: #bbbbbb !important; font-weight: 400; line-height: 20px; font-size: 12px; margin-top: 0px; margin-bottom: 0px; padding-top: 0; padding-bottom: 0; text-decoration: none;"
                                  href="http://divvy.nl" target="_blank">divvy.nl</a>
                                </p>
                              </td>
                            
                              <td style="text-align: center; padding-right: 40px; padding-bottom: 20px;" width="220">
                                <p style="color: #bbbbbb !important; font-weight: 400; line-height: 20px; font-size: 12px; margin-top: 0px; margin-bottom: 0px; padding-top: 0; padding-bottom: 0; font-family: Helvetica, Arial, sans-serif;">
                                  <a style="color: #bbbbbb !important; font-weight: 400; line-height: 20px; font-size: 12px; margin-top: 0px; margin-bottom: 0px; padding-top: 0; padding-bottom: 0; text-decoration: none;"
                                  href="" target="_blank">info@divvy.nl</a>
                                </p>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <!--end closing section-->
                      </td>
                    </tr>
                    <!--end Email sections-->
                  </tbody>
                </table>
                <!--end body-->
              </td>
            </tr>
          </tbody>
        </table>
        <!--end container table-->
        <!--Footer Container-->
        <table style="background: #e8e8e8; line-height: 1.4em;" border="0" cellspacing="0"
        cellpadding="0" width="98%" align="center">
          <tbody>
            <tr width="600">
              <td width="600">
                <!--Email Closer-->
                <table id="email-signoff" style="margin-bottom: 50px; background: #e8e8e8;" border="0"
                cellspacing="0" cellpadding="0" width="600" align="center">
                  <tbody>
                    <tr width="600">
                      <td style="padding-top: 40px; text-align: center;" colspan="3" width="600">
                        <img style="" src="" alt="Divy Logo" width="31" height="30" />
                        <p style="color: #31373a; font-weight: 300; line-height: 18px; font-size: 11px; margin-top: 15px; margin-bottom: 6px; padding-top: 0; padding-bottom: 0; font-family: Helvetica, Arial, sans-serif;">Volg ons op social media om op de hoogte te blijven:</p>
                      </td>
                    </tr>
                    <!--Social Links-->
                    <tr width="600">
                      <td style="padding-left: 152px; text-align: left;" width="85">
                        <!--Twitter-->
                        <a style="color: #31373a; font-weight: bold; line-height: 18px; text-decoration: none; font-size: 14px; margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; font-family: Helvetica, Arial, sans-serif;"
                        title="Follow Sprout Social on Twitter" href="" target="_blank">
                          <img style="color: #56bbe7; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-right: 8px;"
                          src="http://d672eyudr6aq1.cloudfront.net/img/email/shared_email_images/footer-icon-twitter.png"
                          alt="Icon" width="15" height="12" />Twitter</a>
                      </td>
                      <td style="padding-left: 20px; padding-right: 20px; text-align: center;" width="85">
                        <!--Facebook-->
                        <a style="color: #31373a; font-weight: bold; line-height: 18px; text-decoration: none; font-size: 14px; margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; font-family: Helvetica, Arial, sans-serif;"
                        title="Like Sprout Social on Facebook" href="" target="_blank">
                          <img style="color: #627aad; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-right: 8px;"
                          src="http://d672eyudr6aq1.cloudfront.net/img/email/shared_email_images/footer-icon-facebook.png"
                          alt="Icon" width="6" height="12" />Facebook</a>
                      </td>
                      <td style="padding-right: 152px; text-align: right;" width="85">
                        <!--Google Plus-->
                        <a style="color: #31373a; font-weight: bold; line-height: 18px; text-decoration: none; font-size: 14px; margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; font-family: Helvetica, Arial, sans-serif;"
                        title="Follow Sprout Social on Google+" href="" target="_blank">
                          <img style="color: #e36f60; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-right: 8px;"
                          src="http://d672eyudr6aq1.cloudfront.net/img/email/shared_email_images/footer-icon-google.png"
                          alt="Icon" width="11" height="12" />Google+</a>
                      </td>
                    </tr>
                    <!--Address and Unsubscribe-->
                    <tr width="600">
                      <td style="text-align: center; padding-top: 20px;" colspan="3" width="600">
                        <p style="color: #31373a; font-weight: 300; line-height: 18px; font-size: 11px; margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; font-family: Helvetica, Arial, sans-serif;">
                          <a style="text-decoration: none; font-family: Helvetica, Arial, sans-serif; color: #31373a;"
                          title="View Map" href="" target="_blank">Divvy</a>
                          <a class="mktNoTrack" style="color: #31373a; font-weight: bold; text-decoration: none; font-family: Helvetica, Arial, sans-serif;" title="Uitschrijven voor Divvy e-mails" href="" target="_blank">Afmelden nieuwsbrief</a>
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <!--end Email Closer-->
              </td>
            </tr>
          </tbody>
        </table>
        <!--end Foot Container-->
      </div>
    </div>
    <IMG SRC="http://email-link.sproutsocial.com/trk?t=1&mid=NjI4LUhDRC04NDU6MTEwMjoxNjMzOjIzMTU0OjA6MTY4NDo3OjIwODI5NzA6bGF1cmVuQGxpdG11cy5jb20%3D"
    WIDTH="1" HEIGHT="1" BORDER="0" ALT="" />&nbsp;</body>

</html>