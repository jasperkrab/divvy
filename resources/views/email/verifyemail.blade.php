<p>Help us secure your Divvy account by verifying your email address ({{ $user->email }}). This lets you access all of Divvy's features.
							</p>
<a href="{{ route('sendemaildone', ["email" => $user->email, "verifyToken" => $user->verifyToken]) }}">Verify email address</a>
