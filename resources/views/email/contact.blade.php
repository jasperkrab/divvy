<p>
	You have received a new message from your website contact form.
</p>
<p>
	Here are the details:
</p>
<ul>
	<li>Naam: <strong>{{ $name }}</strong></li>
	<li>E-mail: <strong>{{ $email }}</strong></li>
	<li>Onderwerp: <strong>{{ $subject }}</strong></li>
</ul>
<hr>
<p>
	@foreach ($messageLines as $messageLine)
		{{ $messageLine }}<br>
	@endforeach
</p>
<hr>