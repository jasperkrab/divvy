<?php

namespace App\Jobs;

use App\Models\Post;
use App\Models\Category;

class BlogIndexData extends Job
{
    protected $category;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()($category)
    {
        $this->categorie = $category;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->category) {
            return $this->categoryIndexData($this->category);
        }

        //if ($this->tag) {
        //  return $this->tagIndexData($this->tag);
        //}
    }

        return $this->normalIndexData();
    }

    /**
    * Return data for normal index page
    *
    * @return array
    */
    protected function normalIndexData()
    {
        $posts = Post::with('categories')
        ->where('status', 1)
        ->orderBy('id', 'desc')
        ->paginate(10);

        return [
            'posts' => $posts,
            'category' => null,
        ];
    }

    /**
    * Return data for a category index page
    *
    * @param string $category
    * @return array
    */
    protected function categoryIndexData($category)
    {
        $category = Category::where('category', $category)->firstOrFail();
        $reverse_direction = (bool)$category->reverse_direction;

        $posts = Post::whereHas('categories', function ($q) use ($category) {
                $q->where('category', '=', $category->category);
            })
        ->where('is_draft', 1)
        ->orderBy('published_at', $reverse_direction ? 'asc' : 'desc')
        ->simplePaginate(config('blog.posts_per_page'));
        $posts->addQuery('category', $category->category);

        $page_image = $category->page_image ?: config('blog.page_image');

        return [
            'posts' => $posts,
            'category' => $category,
            ];
        }
    }
}
