<?php

namespace App\Jobs;

use App\Models\Post;
use App\Models\Tag;
use App\Models\Category;


use Auth;

class PostFormFields extends Job
{
    /**
    * The slug (if any) of the Post row
    *
    * @var integer
    */
    protected $slug;

    /**
    * List of fields and default value for each field
    *
    * @var array
    */
    protected $fieldList = [
        'status' => "0",
        'meta_description' => '',
        'tags' => [],
        'categories' => '',
        'title' => '',
];

/**
* Create a new command instance.
*
* @param integer $slug
*/
public function __construct($slug = null)
{
    $this->slug = $slug;
}

/**
* Execute the command.
*
* @return array of fieldnames => values
*/
public function handle()
{
    $fields = $this->fieldList;

    if ($this->slug) {
        $fields = $this->fieldsFromModel($this->slug, $fields);
    }

    foreach ($fields as $fieldName => $fieldValue) {
        $fields[$fieldName] = old($fieldName, $fieldValue);
    }

    //populate form fields
    return array_merge(
        $fields,
        ['allcategories' => Category::pluck('category')->all()],
        ['allTags' => Tag::pluck('tag')->all()]
    );
}

    /**
     * Return the field values from the model
     *
     * @param integer $slug
     * @param array $fields
     * @return array
     */
    protected function fieldsFromModel($slug, array $fields)
    {
        $post = Post::whereSlug($slug)->firstOrFail();
        $fieldNames = array_keys(array_except($fields, ['tags', 'categories']));

        $fields = ['slug' => $slug];
        foreach ($fieldNames as $field) {
            $fields[$field] = $post->{$field};
        }

        $fields['tags'] = $post->tags()->pluck('tag')->all();
        $fields['categories'] = $post->category()->pluck('category')->all();
        
        //returns post
        $fields['post'] = $post;

        return $fields;
    }
}