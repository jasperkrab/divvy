<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Post extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /* Posts have assigned users */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

     /**
     * post step relation
     *
     */
    public function steps()
    {
        return $this->hasMany('App\Models\Step');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    /**
     * post tags relation
     *
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'posts_tags');
    }

    //tags sync
    public function syncTags(array $tags)
    {
        Tag::addNeededTags($tags);

        if (count($tags)) {
            $this->tags()->sync(
                Tag::whereIn('tag', $tags)->pluck('id')->all()
            );
            return;
        }

        $this->tags()->detach();
    }

    /**
     * Set the title attribute and automatically the slug
     *
     * @param string $value
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;

        if (! $this->exists) {
            $this->setUniqueSlug($value, '');
        }
    }

     /**
     * Recursive routine to set a unique slug
     *
     * @param string $title
     * @param mixed $extra
     */
    public function setUniqueSlug($title, $extra)
    {
        $slug = str_slug($title.'-'.$extra);

        if (static::whereSlug($slug)->exists()) {
            $this->setUniqueSlug($title, $extra + 1);
            return;
        }

        $this->attributes['slug'] = $slug;
    }

    public function addComment($body, $parent_id = NULL)
    {
        Comment::create([
            'post_id' => $this->id,
            'user_id' => Auth::user()->id,
            'parent_comment_id' => $parent_id,
            'body' => $body,
            'approved' => 1,
        ]);
    }

    public function addStep()
    {
        Step::create([
            'post_id' => $this->id,
            'step_nr' => 1,
            'title' => NULL,
            'body' => NULL,
        ]);
    }


     /**
    * xxxxxxxxxx
    */

     // public function getTumbImage()
     // {
     //    if(file_exists('storage/posts/'. $post->id .'/img/' . $post->post_cover)){
     //        return 'storage/posts/'. $post->id .'/img/' . $post->post_cover;
     //    } else {
     //        return 'storage/posts/default/img/tumb.jpg';
     //    }
     // }

}
