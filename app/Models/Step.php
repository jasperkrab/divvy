<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    //
    protected $fillable = [
		'post_id', 'step_nr', 'body',
	];


    public function posts()
	{
		return $this->belongsTo('App\Models\Post');
	}
}
