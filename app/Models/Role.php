<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

     /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get users with a certain role
     */
  
    public function users()
    {
      return $this->belongsToMany('App\Models\User', 'users_roles', 'role_id', 'user_id');
   }
}