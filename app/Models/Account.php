<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'last_name', 'username', 'email', 'location', 'avatar',
    ];
}