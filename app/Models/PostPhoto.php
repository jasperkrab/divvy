<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Postphoto extends Model
{
    //
    protected $fillable = [
		'post_id', 'user_id', 'filename',
	];


    public function posts()
	{
		return $this->belongsTo('App\Models\Post');
	}
}
}
