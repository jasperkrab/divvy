<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	protected $fillable = [
		'tag', 'meta_description',
	];

	public function posts()
	{
		return $this->belongsToMany('App\Models\Post', 'posts_tags');
	}

	public static function addNeededTags(array $tags)
	{
		if (count($tags) === 0) {
			return;
		}

		$found = static::whereIn('tag', $tags)->pluck('tag')->all();

		foreach (array_diff($tags, $found) as $tag) {
	  		static::create([
			    'tag' => $tag,
			    'meta_description' => '',
		 	]);
		}
	}

	public function getRouteKeyName()
	{
		return 'tag';
	}
	
}
