<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
		'category', 'meta_description',
	];

	public function posts(){

    	return $this->hasMany('App\Models\Post');
    }

    //caegory name instead of id
    public function getRouteKeyName()
	{
		return 'category';
	}
	
}
