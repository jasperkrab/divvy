<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;

use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Using Closure based composers...
    //     $this->app['view']->composer('dashboard', function ($view) {
    //         $view->title = 'Laracasts';
    //     });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        //dutch time
        Carbon::setLocale('nl');
    }
}
