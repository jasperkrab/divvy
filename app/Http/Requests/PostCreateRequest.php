<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Providers\CustomValidatorServiceProvider;

use Auth;

class PostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|alpha_spaces|min:3',
            'category' => 'required',
            'tags' => 'required',
            'step-title.*' => 'required|min:3',
            'step-body.*' => 'required|min:3',
        ];

        // foreach($this->request->get('step-title') as $key => $val)
        // {
        //     $key = 1 + $key; //because steps start at 1 not 0
        //     $rules['step-title'.$key] = 'required';
        //     $rules['step-body'.$key] = 'required';
        // }

        return $rules;

    }

    /**
     * Return the fields and values to create a new post from
     */
    public function postFillData()
    {
        return [
            'title' => $this->title,
            'status' => 0,
            'user_id' => Auth::user()->id,
            //'category_id' => $this->category,
            'post_cover' => 'tumb.jpg',
        ];
    }

    // public function messages()
    // {
    //     $messages = [];
    //     foreach($this->request->get('step-body') as $key => $val)
    //     {
    //         $messages['step-body.'.$key.'.max'] = 'The field labeled '.$key.'" must be less than :max characters.';
    //     }

    //     return $messages;
    // }

}