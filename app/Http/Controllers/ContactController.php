<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactUsRequest;

class ContactController extends Controller
{
	public function getContact(){
		return view('pages/contact');
	}
	
	public function sendContactInfo(ContactUsRequest $request)
	{
		$data = $request->only('name', 'email', 'subject');
		$data['messageLines'] = explode("\n", $request->get('message'));

		Mail::send('email.contact', $data, function ($message) use ($data) {
			$message->subject($data['subject'])
			->to(config('app.contact_email'))
			->replyTo($data['email']);
		});

		flash()->success('Bedankt, uw bericht is verzonden.');
		return back();
	}
}
