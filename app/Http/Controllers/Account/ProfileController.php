<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getProfile($username)
    {
        //find the user in the database from url
        $user = User::whereUsername($username)->firstOrFail();
        $posts = $user->posts->where('status', 1);

    	return view('user.index', compact('user', 'posts'));
    }

    public function getPosts($username)
    {
        $user = User::whereUsername($username)->firstOrFail();
        $posts = $user->posts->where('status', 1);

        return view('user.posts', compact('user', 'posts'));
    }
}
