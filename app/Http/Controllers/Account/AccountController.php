<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Providers\CustomValidatorServiceProvider;

use App\Models\User;
use Auth;

use Purifier;
use Image;
use Storage;

use Hash;

class AccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        //find the user in the database and save as a variable
        $user = Auth::user();

    	return view('user.account.edit')->withUser($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();

        //validate
        $this->validate($request, [
            'first_name'    => 'nullable|alpha_spaces|max:100',
            'last_name'     => 'nullable|alpha_spaces|max:100',
            'username'      => 'max:255',
            'email'         => 'required|email|unique:users,email,'.$user->id,
            'location'      => 'nullable|max:255',
            'avatar'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',

            //voorbeelden
            //'email'         =>  Rule::unique('users')->ignore($user->id),
        ]);

        //update the data
        $user->first_name = ucfirst($request->input('first_name'));
        $user->last_name = ucfirst($request->input('last_name'));
        $user->email = $request->input('email');
        $user->location = $request->input('location');

        //image uploading
        if ($request->HasFile('avatar')) {

            $file  = $request->file('avatar');
            $fileName = $user->id . '.jpg';
            $path = storage_path('app/public/img/avatars/');

            $img = Image::make($file->getRealPath());
            $img->fit(250, 250, function ($constraint) {
                $constraint->upsize();
            })->encode('jpg')->save($path . $fileName);

            //save in db
            $user->avatar = $fileName;
        }

        //save the data to the database
        $user->save();

        //flash message
        flash()->success('Account updated!');

        //redirect
        return redirect()->route('account');
    }



    public function password()
    {
        $user = Auth::user();

        return view('user.account.password')->withUser($user);

    }

    public function passwordUpdate(Request $request)
    {
        $user = Auth::user();
        $hashedPassword = $user->password;

        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|min:6|different:old_password|confirmed',
        ]);

        if (Hash::check($request->old_password, $hashedPassword)) {
            //Change the password
            $user->password = bcrypt($request->input('password'));
            $user->save();
            
            //flash
            flash()->success('Password has been updated!');
            //redirect
            return redirect()->route('account.password');
        }

        flash()->warning('Het oude wachtwoord klopt niet');

        return back();
    }


    public function deleteAccount()
    {
        $user = Auth::user();

        return view('user.account.delete')->withUser($user);
    }

    public function destroy()
    {
        $user = Auth::user();

        //delete users posts and comments
        //

        //delete user avatar
        Storage::delete('public/img/avatars/' . $user->avatar); 

        //destroy
        $user->delete();

        flash()->success('Je account is succesvol opgezegd, tot ziens!');

        return redirect('/');
    }
}
