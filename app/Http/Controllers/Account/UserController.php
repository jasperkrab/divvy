<?php

namespace App\Http\Controllers\Account;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getMessages()
    {
        $user = Auth()->user();
    	return view('user.messages', compact('user'));
    }

    public function getDrafts()
    {
        $user = Auth()->user();
        $posts = $user->posts->where('status', 0);

        return view('user.drafts', compact('user', 'posts'));
    }
}
