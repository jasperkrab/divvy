<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;

use App\Jobs\PostFormFields;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;

use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Tag;
use App\Models\Step;
use App\Models\Category;

use Auth;
use Session;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        //validate the data
        $this->validate($request, [
            'title' => 'required|alpha_spaces|min:3|max:100',
        ]);

        //create post
        $post = Post::create([
            'status' => NULL,
            'user_id' => Auth::user()->id,
            'meta_description' => $request->title,
            'category_id' => NULL,
            'title' => $request->title,
            'post_cover' => 'tumb.jpg',
            'step_count' => 1,
        ]);

        //create first step
        $post->addStep();

        //give postcontroller the right slug
        $slug = $post->slug;

        return redirect()->route('posts.edit', compact('slug'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $data = $this->dispatch(new PostFormFields($slug));

        return view('posts.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, $slug)
    {
        $this->validate($request, [
            'step-title-.*' => 'required|min:3',
            'step-body-.*' => 'required|min:3',
        ]);

        //
        $post = Post::whereSlug($slug)->firstOrFail();
        $post->fill($request->postFillData());

        //$post->setTitleAttribute($post->title);
        //$post->setUniqueSlug($post->title, '');

        //get category id
        $category = $request->category;
        $post->category_id = Category::whereCategory($category)->firstOrFail()->id;

        //save steps in db
        foreach($request->get('step-title-') as $key => $val) {

            $step_nr = $key + 1;
            $step = Step::firstOrNew([
                'post_id' => $post->id,
                'step_nr' => $step_nr
            ]);
            //$step = Step::wherePost_id($post->id)->whereStep_nr($step_nr)->firstOrFail();

            $step->post_id = $post->id;
            $step->step_nr = $step_nr;
            $step->title = $request->input('step-title-')[$key];
            $step->body = $request->get('step-body-')[$key];

            $step->save();

            //save the number of steps
            $post->step_count = $step->step_nr;
        }

        $post->save();
        $post->syncTags($request->get('tags', []));

        if ($request->has('save')) {
            flash()->success('De post is opgeslagen.');
            return redirect()->back();

        } else if ($request->has('publish')) {
            return redirect()->route('user.posts', Auth::user()->username);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd('removed');
        //
        $post = Post::whereSlug($slug)->firstOrFail();
        $post->tags()->detach();
        //Storage::delete($post->image);
        
        $post->delete();

        flash()->success('Post is met succes verwijderd!');

        //belangrijk om terug te komen bij eigen posts
        $user = Auth::user();

        return redirect()->route('user.posts', compact('user'));
    }
}
