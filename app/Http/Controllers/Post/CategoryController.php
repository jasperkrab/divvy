<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin');
    }

	protected $fields = [
		'category' => '',
		'meta_description' => '',
	];

	public function index()
	{
		$categories = Category::all();

		return view('admin.category.index')->withCategories($categories);
	}

	public function create()
	{

		return view('admin.category.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
            'category' => 'required|min:3|max:144|unique:categories,category',
            'meta_description' => 'min:3|max:255',
        ]);

        $category = new Category;

        $category->category = $request->category;
		$category->meta_description = $request->meta_description;

		$category->save();

		flash()->success("De '$category->category' is aangemaakt.");

		return redirect()->route('categories.index');
	}

	public function edit($id)
	{
		$category = Category::findOrFail($id);
		$data = ['id' => $id];
		
		foreach (array_keys($this->fields) as $field) {
			$data[$field] = old($field, $category->$field);
		}

		return view('admin.category.edit', $data);
	}

	public function update(Request $request, $id)
	{
		$category = Category::findOrFail($id);

		$this->validate($request, [
            'category' => 'required|min:3|max:144|unique:categories,category,'.$category->id,
            'meta_description' => 'min:3|max:255',
        ]);

		$category->category = $request->category;
		$category->meta_description = $request->meta_description;

		$category->save();

		flash()->success('Aanpassingen zijn opgeslagen.');

		return redirect()->route('categories.index');
	}

	public function destroy($id)
	{
		$category = Category::findOrFail($id);
		$category->posts()->detach();
		
		$category->delete();

		flash()->success("De '$category->category' is verwijderd.");

		return redirect()->route('categories.index');
	}
}
