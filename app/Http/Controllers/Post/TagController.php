<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;

use App\Http\Requests\TagCreateRequest;

use Illuminate\Http\Request;
use App\Models\Tag;

class TagController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin');
    }

	protected $fields = [
		'tag' => '',
		'meta_description' => '',
	];

	public function index()
	{
		$tags = Tag::all();

		return view('admin.tag.index')->withTags($tags);
	}

	public function create()
	{
		$data = [];
		foreach ($this->fields as $field => $default) {
			$data[$field] = old($field, $default);
		}

		return view('admin.tag.create', $data);
	}

	public function store(TagCreateRequest $request)
	{
		$tag = new Tag();

		foreach (array_keys($this->fields) as $field) {
			$tag->$field = $request->get($field);
		}

		$tag->save();

		flash()->success("De '$tag->tag' is aangemaakt.");

		return redirect()->route('tags.index');
	}

	public function edit($id)
	{
		$tag = Tag::findOrFail($id);
		$data = ['id' => $id];
		
		foreach (array_keys($this->fields) as $field) {
			$data[$field] = old($field, $tag->$field);
		}

		return view('admin.tag.edit', $data);
	}

	public function update(Request $request, $id)
	{
		$tag = Tag::findOrFail($id);

		$this->validate($request, [
            'tag' => 'required|min:3|max:144|unique:tags,tag,'.$tag->id,
            'meta_description' => 'nullable|min:3|max:255',
        ]);

		$tag->tag = $request->tag;

		foreach (array_keys(array_except($this->fields, ['tag'])) as $field) {
			$tag->$field = $request->get($field);
		}

		$tag->save();

		flash()->success('Aanpassingen zijn opgeslagen.');

		return redirect()->route('tags.index');
	}

	public function destroy($id)
	{
		$tag = Tag::findOrFail($id);
		$tag->posts()->detach();
		
		$tag->delete();

		flash()->success("De '$tag->tag' is verwijderd.");

		return redirect()->route('tags.index');
	}
}
