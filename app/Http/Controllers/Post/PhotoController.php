<?php

namespace App\Http\Controllers\Post;

use App\Http\Requests\PhotoUploadRequest;
use App\Http\Controllers\Controller;

class PhotoController extends Controller
{
    public function upload()
    {
        return view('upload_form');
    }
 
    public function Submit(PhotoUploadRequest $request)
    {
        $photo = Post_photo::create($request->all());
        foreach ($request->photos as $photo) {
            $filename = $photo->store('photos');
            ProductsPhoto::create([
                'post_id' => $post->id,
                'filename' => $filename
            ]);
        }
        return 'Upload successful!';
    }
}
