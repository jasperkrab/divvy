<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Step;
use App\Models\Post;

class StepController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        //increment step count
        $i = $post->step_count;
        $i++;
        $post->step_count = $i;


        $post->save();

        //add step
        $step = new Step;

        $step->post_id = $post->id;
        $step->step_nr = $i;

        $step->save();

        //return 'Step created';
    }

    public function destroy($id)
    {  
        $step = Step::whereId($id)->firstOrFail();

        //change post steps
        $post = Post::whereId($step->post_id)->firstOrFail();
        $steps = $post->step_count;
        $post->step_count = $steps - 1;
        $post->save();

        $step->delete();

        return back();
    }
}
