<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;

use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    // public function getIndex(){
    // 	$posts = Post::orderBy('created_at', 'desc')
    // 		->paginate(15);

    // 	return view('blog.index', compact('posts'));
    // }

    public function getSingle($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        $comments = $post->comments->sortBy('created_at')->take(10);

        return view('blog.single', compact('post', 'comments'));
    }

    public function getCategory(Category $category){

        $posts = $category->posts;
        $category = $category->category;

        return view('blog.category.index', compact('posts', 'category'));
    }

     public function getTag(Tag $tag){

        $posts = $tag->posts;
        $tag = $tag->tag;

        return view('blog.tag.index', compact('posts', 'tag'));
    }
}
