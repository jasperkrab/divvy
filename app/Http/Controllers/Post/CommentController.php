<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Comment;

use Auth;

class CommentController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $slug)
    {
    	//find post in db
    	$post = Post::whereSlug($slug)->firstOrFail();

    	//validate
    	$this->validate(request(), [
    		'body' => 'required|min:2|max:2000',
    	]);

    	//add comment to the db
    	$post->addComment(request('body'));

    	//fash message
        flash()->success('Uw reactie is geplaatst!');

    	return back();
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::whereId($id)->firstOrFail();

        $comment->body = $request->edit_body;
        $comment->save();

        //fash message
        flash()->success('Uw reactie is gewijzigd!');

        return back();
    }

    public function reply(Request $request, $slug, $id)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        //validate
        $this->validate(request(), [
            'reply_body' => 'required|min:2|max:2000',
        ]);

        //add reply to the db
        $post->addComment(request('reply_body'), $id);

        //fash message
        flash()->success('Uw reactie is geplaatst!');

        return back();
    }

    public function destroy($id)
    {
    	//
        $comment = Comment::whereId($id)->firstOrFail();
        $comment->delete();

        flash()->success('De reactie is met succes verwijderd!');

        return back();
    }
}
