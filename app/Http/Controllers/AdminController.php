<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Auth;

class AdminController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Admin Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating admin functions
    |
    */

	public function __construct()
	{
		$this->middleware('admin');
		//$this->middleware('admin', ['except' => 'test']);
	}

	public function getIndex()
	{
		$users = User::orderBy('id', 'asc')->paginate(10);

		return view('admin.index')->withUsers($users);
	}

	public function postAdminAssignRoles(Request $request)
	{
		$user = User::where('email', $request['email'])->first();
		$user->roles()->detach();

		if ($request['role_admin']){
			$user->assignRole('admin');
		}

		if ($request['role_subscriber']){
			$user->assignRole('subscriber');
		}

		if ($request['role_user']){
			$user->assignRole('user');
		}

		flash()->success('Roles have been edited!');

		return redirect()->back();
	}

}
