<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Mail;

use App\Models\Post;
use App\Models\User;
/**
* 
*/

class PagesController extends Controller {

	public function getIndex()
	{
		// $users = User::all();

		$posts = Post::orderBy('created_at', 'desc')
    		->paginate(15)->where('status', 1);

		return view('pages/home', compact('posts', 'getTumbImage'));
	}

	public function getAbout(){

		$company = 'Divvy';//how to pass data

		return view('pages/over-ons')->with("companyname", $company); //alternatief = withCompanyname($company);
	}

	public function getHowto(){

		return view('pages/werkwijze');
	}
}