<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Role;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Mail;
use Auth;
use App\Mail\verifyEmail;

//register function
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

//redirect home
use App\Models\Post;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    //onUpdate put this in the RegisterUsers file
    //return redirect(route('verifyEmailFirst'));

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /*
     * framework override
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //aanpassing
        return redirect(route('login'));

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255|alpha_dash|max:20|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'username' => ucfirst($data['username']),
            'avatar' => 'default.jpg',
            'verifyToken' => Str::random(40),
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        //assign user role
        $user->assignRole('user');

        $thisUser = User::findOrFail($user->id);
        $this->sendEmail($thisUser);

        flash()->success('Geregistreerd! Bevestig je email adres om je account te activeren')->important();

        return $user;
    }

    public function sendEmail($thisUser)
    {
        Mail::to($thisUser['email'])->send(new verifyEmail($thisUser));
    }

    public function sendEmailDone($email, $verifyToken)
    {
        $user = User::where(['email' => $email, 'verifyToken' => $verifyToken])->first();
        if($user){
            User::where([
                'email' => $email,
                'verifyToken' => $verifyToken
            ])->update([
                'status' => '1',
                'verifyToken' => 'NULL'
            ]);

            Auth::login($user);

            flash()->success('Je bent succesvol ingelogd!');

            //see pagescontroller
            $posts = Post::orderBy('created_at', 'desc')
            ->paginate(15)->where('status', 1);

            return view('pages/home', compact('posts', 'getTumbImage'));

        } else {
            flash()->danger('Oeps! Er ging iets fout.');

            //see pagescontroller
            $posts = Post::orderBy('created_at', 'desc')
            ->paginate(15)->where('status', 1);

            return view('pages/home', compact('posts', 'getTumbImage'));
        }
    }
}
