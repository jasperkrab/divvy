<?php

namespace App\Http\Controllers;


use App\Jobs\PostFormFields;
use App\Http\Requests\PostCreateRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Tag;
use App\Models\Step;
use App\Models\Category;

use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function index()
    // {
    //     $user = Auth::user();

    //     //all post masonry grid
    //     return view('users.index');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->dispatch(new PostFormFields());

        return view('posts.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {

        //dd($request->all());
    
        $post = Post::create(
            $request->postFillData()
        );

        $post->syncTags($request->get('tags', []));

        //save steps in db
        foreach($request->get('step-title') as $key => $val)
        {
            $step = new Step;

            $step->post_id = $post->id;
            $step->step_nr = 1 + $key;
            $step->title = $request->input('step-title')[$key];
            $step->body = $request->get('step-body')[$key];

            $step->save();
        }

        //count the steps
        $post->step_count = $step->step_nr;

        //store data in the database
        $post->save();

        if ($request->has('save')){

            flash()->success('Opgeslagen');
            //go back
            return redirect()->back();

        } else if ($request->has('publish')) {
            //opslaan als gepubliceerd
            $post->status = 1;

            //store data in the database
            $post->save();

            flash()->success('De post is successvol gepubliceerd!');

            //show post
            return redirect()->route('posts.show', $post->slug);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        $data = $this->dispatch(new PostFormFields($slug));

        $post->save();

        return view('posts.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostUpdateRequest $request, $id)
    {
        //
        $post = Post::findOrFail($id);
        $post->fill($request->postFillData());

        $post->save();
        $post->syncTags($request->get('tags', []));

        if ($request->action === 'continue') {

            flash('success', 'Post saved.');
            return redirect()->back();
        }

        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::findOrFail($id);
        $post->tags()->detach();
        //Storage::delete($post->image);
        
        $post->delete();

        flash('success', 'Post is met succes verwijderd!');

        //belangrijk om terug te komen bij eigen posts
        $user = Auth::user();

        return redirect()->route('user.posts', compact('user'));
    }
}
