<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Authentication Routes
Auth::routes();

//Route::get('verifyEmailFirst', 'Auth\RegisterController@verifyEmailFirst')->name('verifyemailfirst');
Route::get('verify/{email}/{verifyToken}', 'Auth\RegisterController@sendEmailDone')->name('sendemaildone');

//pages
Route::get('over-ons', 'PagesController@getAbout')->name('about');
Route::get('werkwijze', 'PagesController@getHowto')->name('howto');

//contact
Route::get('contact', 'ContactController@getContact')->name('contact');
Route::post('contact', 'ContactController@sendContactInfo')->name('contact.sent');

//admin
Route::prefix('admin')->group(function(){
	Route::get('/', 'AdminController@getIndex')->name('admin');
	Route::post('/', 'AdminController@postAdminAssignRoles')->name('admin.assign');
	Route::resource('/categories', 'Post\CategoryController', ['except' => 'show']);
	Route::resource('/tags', 'Post\TagController', ['except' => 'show']);
});

//homepage
Route::get('/', 'PagesController@getIndex');

//divvy collections
//Route::get('collection', 'Post\BlogController@getIndex')->name('category.all');
Route::get('blog/{slug}', 'Post\BlogController@getSingle')->name('blog.single');

Route::get('categories/{category}', 'Post\BlogController@getCategory')->name('category.index');
Route::get('tags/{tag}', 'Post\BlogController@getTag')->name('tag.index');


//post routes
	// Route::post('posts/{post}', 'Post\PostController@update')->name('posts.update');
	// Route::get('posts/{post}/edit', 'Post\PostController@edit')->name('posts.edit');
	// Route::post('posts', 'Post\PostController@store')->name('posts.store');

	Route::resource('posts', 'Post\PostController', ['except' => ['index', 'create']]);
	//
	Route::post('posts/{slug}/comments', 'Post\CommentController@store')->name('comments.store');
	Route::put('posts/{slug}/comments/update', 'Post\CommentController@update')->name('comments.update');
	Route::post('posts/{slug}/comments/reactie/{id}', 'Post\CommentController@reply')->name('comments.reply');

	Route::delete('posts/{slug}/comments/verwijderen', 'Post\CommentController@destroy')->name('comments.destroy');

	//steps
	Route::post('posts/{post}/steps', 'Post\StepController@store')->name('steps.store');
	Route::post('posts/{post}/steps/verwijderen', 'Post\StepController@destroy')->name('steps.destroy');
//

//account
Route::prefix('account')->group(function(){	
	Route::get('/', 'Account\AccountController@edit')->name('account');
	Route::post('/update', 'Account\AccountController@update')->name('account.update');

	//account wachtwoord
	Route::get('/wachtwoord', 'Account\AccountController@password')->name('account.password');
	Route::put('/wachtwoord/update/{user}', 'Account\AccountController@passwordUpdate')->name('password.update');

	//delete account
	Route::get('account_verwijderen', 'Account\AccountController@deleteAccount')->name('delete.account');
	Route::post('account_verwijderen', 'Account\AccountController@destroy')->name('account.destroy');
});

//imageupload
// Route::get('/upload', 'ImageProcessController@upload');
// Route::post('/upload', 'ImageProcessController@postupload');
// Route::post('/cover-image-save', 'ImageProcessController@postimgAdjustpostion');

//profile routes
Route::prefix('{username}')->group(function(){
	Route::get('/profiel', 'Account\ProfileController@getProfile')->name('profile');
	Route::get('/profiel/posts', 'Account\ProfileController@getPosts')->name('user.posts');
});

//user routes
Route::get('drafts', 'Account\UserController@getDrafts')->name('user.drafts');
Route::get('berichten', 'Account\UserController@getMessages')->name('messages');


